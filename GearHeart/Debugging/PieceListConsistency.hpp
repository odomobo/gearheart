#pragma once

#include "../Interface.hpp"

class PieceListConsistency : public DebuggingHooksBase
{
public:
    inline static std::string Name{"PieceListConsistency"};

    void AfterBoardPieceModified(Board const & board) override
    {
        // first check that each piece on the board has a piece in the piece list
        for (int rank = 0; rank < 8; rank++)
        {
            for (int file = 0; file < 8; file++)
            {
                Coord coord{file, rank};
                Piece piece = board[coord];
                if (piece != Space)
                {
                    //PieceAndIndex pieceAndIndex = board._board[coord.GetArrayIndex()];
                    Piece piece = board[coord];

                    piece_list_t const & pieceList = board._pieceLists.GetPieceList(piece);

                    std::optional<size_t> index = board._pieceLists.GetIndex(piece, coord);
                    if (!index.has_value())
                        throw GhException{"Piece does not have index in piece list"};

                    // if we reach here, I guess the piece is good
                }
            }
        }

        for (Color color : Colors)
        {
            for (PieceType type : PieceTypes)
            {
                Piece piece = color + type;

                auto & pieceList = board.GetPieceList(piece);
                for (int index = 0; index < std::size(pieceList); index++)
                {
                    Coord coord = pieceList[index];
                    if (coord.IsInvalid())
                        throw GhException{"Somehow an invalid coord snuck into our piece list"};

                    if (board[coord] != piece)
                        throw GhException{"Piece list coord is pointing to an invalid value on the board"};

                    // if we reach here, the piecelist coord is good
                }
            }

            // check the "None" piece lists
            if (std::size(board._pieceLists.GetPieceList(color, PieceTypeNone)) > 0)
                throw GhException{"Piecelist for \"None\" type should be empty, but it contains data"};
        }

        //_Info("PieceListConsistency::AfterBoardPieceModified Successful");
    }
};
