#pragma once

#include <string>

#include "../Interface.hpp"

class IncrementalConsistency : public DebuggingHooksBase
{
public:
    inline static std::string Name{ "IncrementalConsistency" };

    void AfterMoveApplied(Board const& board) override
    {
        if (board._IncrementalData._ZobristPieces != ZobristHash::FromPieces(board))
        {
            throw GhException{ "Incremental zobrist piece data became corrupted." };
        }

        if (board._IncrementalData._PartialScore != EvaluatePartialScore(board))
        {
            throw GhException{ "Incremental partial score became corrupted." };
        }
    }
};
