#pragma once

#include "../Interface.hpp"

template <typename ...>
class DebuggingHooksImpl : public DebuggingHooksBase
{};

template <typename TCurrent, typename ... TRest>
class DebuggingHooksImpl<TCurrent, TRest...> : public DebuggingHooksImpl<TRest...>
{
    // TODO: static assert to guarantee TCurrent is a DebuggingHooksBase
    using Parent = DebuggingHooksImpl<TRest...>;

    TCurrent _plugin;

public:
    DebuggingHooksImpl() : _plugin{}
    {
        SyncPrintln("info string Debugging plugin enabled: {0}", TCurrent::Name);
    }

    void BoardReset(Board const& board) override
    {
        _plugin.BoardReset(board);
        Parent::BoardReset(board);
    }

    void AfterBoardPieceModified(Board const& board) override
    {
        _plugin.AfterBoardPieceModified(board);
        Parent::AfterBoardPieceModified(board);
    }

    void AfterMoveApplied(Board const& board) override
    {
        _plugin.AfterMoveApplied(board);
        Parent::AfterMoveApplied(board);
    }
};
