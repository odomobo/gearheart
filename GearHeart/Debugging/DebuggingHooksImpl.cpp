
#include "../Interface.hpp"

#include "DebuggingHooksImpl.hpp"
#include "Example.hpp"
#include "PieceListConsistency.hpp"
#include "IncrementalConsistency.hpp"


// Note: if your ENABLED_DEBUGGING_PLUGINS definition is messed up (in Core/Constants), you will get strange errors here
DebuggingHooksImpl< ENABLED_DEBUGGING_PLUGINS > Impl;

DebuggingHooksBase & DebuggingHooks = Impl;
