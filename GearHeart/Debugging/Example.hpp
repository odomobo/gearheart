#pragma once

#include <string>

#include "../Interface.hpp"

class Example : public DebuggingHooksBase
{
public:
    inline static std::string Name{"Example"};

    void BoardReset(Board const& board) override
    {
        SyncPrintln("info string Example debugging plugin received BoardReset()");
    }
};

