#pragma once

#include "Util.hpp"
#include "BaseTypes.hpp"
#include "Core.hpp"
#include "Engine.hpp"

// Data parsing
#include "Interface/ToString.hpp"
#include "Interface/Parse.hpp"
#include "Interface/Fen.hpp"
#include "Interface/ParseMoves.hpp"
#include "Interface/DumpInfo.hpp"
#include "Interface/Perft.hpp"
#include "Interface/CheckAttackedSquares.hpp"

// UCI
#include "Interface/UciCommandProcessor.hpp"
#include "Interface/UciOptions.hpp"
#include "Interface/UciConfig.hpp"
#include "Interface/Uci.hpp"