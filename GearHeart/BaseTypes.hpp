#pragma once

#include "Util.hpp"

#include "BaseTypes/Typedefs.hpp"
#include "BaseTypes/PieceType.hpp"
#include "BaseTypes/Color.hpp"
#include "BaseTypes/Coord.hpp"
#include "BaseTypes/Score.hpp"

#include "BaseTypes/Enums.hpp"

// composite types
#include "BaseTypes/Piece.hpp"
#include "BaseTypes/SimpleMove.hpp"
#include "BaseTypes/TinyMove.hpp"