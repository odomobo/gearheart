#pragma once

// MSVC is the odd one out...
#ifdef _MSC_VER 
#define Unreachable() __assume(false)
#else
#define Unreachable() __builtin_unreachable()
#endif

// You could use this for testing
//#define Unreachable() throw GhException{"Unreachable code reached"};