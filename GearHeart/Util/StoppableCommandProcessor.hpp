#pragma once

#include <condition_variable>
#include <mutex>
#include <atomic>

enum class ProcessorCommand
{
    Search,
    Quit
};

class StoppableCommandProcessor
{
    std::condition_variable _workerSynchronizer;
    std::condition_variable _interfaceSynchronizer;
    std::mutex _synchronizerMutex;

    std::atomic<bool> _runningState{false};
    std::atomic<bool> _ponderingState{false};

    std::atomic<bool> _startSignal{false};
    std::atomic<bool> _stopSignal{false};
    std::atomic<bool> _quitSignal{false};
    
public:
    StoppableCommandProcessor() {}

    ProcessorCommand WorkerStopAndWaitForCommand()
    {
        std::unique_lock<std::mutex> lock{ _synchronizerMutex };
        _runningState = false;
        _ponderingState = false;
        _interfaceSynchronizer.notify_all(); // notify anything waiting on a stop command

        while (!_startSignal && !_quitSignal)
            _workerSynchronizer.wait(lock);

        if (_quitSignal)
        {
            _quitSignal = false; // technically not needed, since we are quitting
            return ProcessorCommand::Quit;
        }
        else // _startSignal
        {
            _startSignal = false;
            _runningState = true;
            return ProcessorCommand::Search;
        }
    }

    bool WorkerIsStopping() const
    {
        return _stopSignal;
    }

    // Non-blocking, reentrant
    void InterfaceStopAndSynchronize()
    {
        std::unique_lock<std::mutex> lock{ _synchronizerMutex };
        _stopSignal = true;
        _ponderingState = false; // we need to disable pondering, because pondering overrides any stopping state
        while (_runningState == true)
        {
            _interfaceSynchronizer.wait(lock);
        }
        _stopSignal = false;
    }

    void InterfaceSearch(bool ponder = false)
    {
        std::unique_lock<std::mutex> lock{ _synchronizerMutex };
        _startSignal = true;
        _ponderingState = ponder;
        _workerSynchronizer.notify_all();
        // no need to wait
    }

    void InterfacePonderHit()
    {
        // no need to lock
        _ponderingState = false;
    }

    bool IsPondering() const
    {
        return _ponderingState;
    }

    void InterfaceQuit()
    {
        std::unique_lock<std::mutex> lock{ _synchronizerMutex };
        _quitSignal = true;
        _ponderingState = false; // we need to disable pondering, because pondering overrides any stopping state
        _workerSynchronizer.notify_all();
        // no need to wait
    }
};