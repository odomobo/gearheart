#pragma once
#include "../BaseTypes/Coord.hpp"

// This wraps around a container to prevent it from being copied. Sometimes you really only want to pass it as reference...
template <typename TElem, typename TContainer>
class NonCopyableContainer
{
    TContainer _container;
public:

    template <typename ... TArgs>
    NonCopyableContainer(TArgs ... targs) : _container{targs...} {}

    NonCopyableContainer(const NonCopyableContainer& other) = delete;
    NonCopyableContainer(NonCopyableContainer&& other) noexcept = delete;
    NonCopyableContainer& operator=(const NonCopyableContainer& other) = delete;
    NonCopyableContainer& operator=(NonCopyableContainer&& other) noexcept = delete;

    auto begin() noexcept -> decltype(_container.begin())
    {
        return _container.begin();
    }

    auto begin() const noexcept -> decltype(_container.begin())
    {
        return _container.begin();
    }

    auto end() noexcept -> decltype(_container.end())
    {
        return _container.end();
    }

    auto end() const noexcept -> decltype(_container.end())
    {
        return _container.end();
    }

    TElem & operator[](size_t pos)
    {
        return _container[pos];
    }

    TElem const & operator[](size_t pos) const
    {
        return _container[pos];
    }

    size_t size() const noexcept
    {
        return _container.size();
    }

    void push_back(TElem const & value)
    {
        _container.push_back(value);
    }

    void push_back(TElem && value)
    {
        _container.push_back(value);
    }

    void pop_back()
    {
        _container.pop_back();
    }

    void clear() noexcept
    {
        _container.clear();
    }

    TElem & back()
    {
        return _container.back();
    }
};