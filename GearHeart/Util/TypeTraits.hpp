#pragma once

#include <type_traits>
#include <algorithm>

namespace gh
{

template <typename...>
struct is_one_of
{
    static constexpr bool value = false;
};

template <typename F, typename S, typename... T>
struct is_one_of<F, S, T...>
{
    static constexpr bool value =
        std::is_same<F, S>::value || is_one_of<F, T...>::value;
};


template <typename...>
struct are_all_subclasses
{
    static constexpr bool value = true;
};

template <typename F, typename S, typename... T>
struct are_all_subclasses<F, S, T...>
{
    static constexpr bool value =
        std::is_base_of<F, S>::value && are_all_subclasses<F, T...>::value;
};


template <typename...>
struct are_all_trivially_destructible
{
    static constexpr bool value = true;
};

template <typename S, typename... T>
struct are_all_trivially_destructible<S, T...>
{
    static constexpr bool value =
        std::is_trivially_destructible<S>::value && are_all_trivially_destructible<T...>::value;
};


template <typename...>
struct max_sizeof
{
    static constexpr size_t value = 0;
};

template <typename S, typename... T>
struct max_sizeof<S, T...>
{
    static constexpr size_t value =
        std::max(sizeof(S), max_sizeof<T...>::value);
};

template <typename...>
struct max_alignof
{
    static constexpr size_t value = 0;
};

template <typename S, typename... T>
struct max_alignof<S, T...>
{
    static constexpr size_t value =
        std::max(alignof(S), max_alignof<T...>::value);
};

}