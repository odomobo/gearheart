#pragma once

#include <exception>
#include <string>

#include "fmt/format.h"

class GhException : public std::exception
{
    std::string const _what;
public:
    GhException(std::string const & message) : _what{message} {}

    template <typename ... TArgs>
    GhException(TArgs ... args) : _what{fmt::format(args...)} {}

    const char * what() const noexcept override
    {
        return _what.c_str();
    }
};

class GhAssertionException : public std::exception
{
    std::string const _file;
    int _line;
    std::string const _message;
    std::string const _what;
public:

    GhAssertionException(std::string const & message) : _file{}, _line{-1}, _message{message}, _what{message}
    {}

    GhAssertionException(std::string const & file, int line, std::string const & message) : _file{file}, _line{line}, _message{message}, _what{fmt::format("{0} [{1}:{2}]", message, file, line)}
    {}

    const char * what() const noexcept override
    {
        return _what.c_str();
    }
};

class NotImplementedException : public std::exception
{
public:
    const char * what() const noexcept override
    {
        return "Not Implemented";
    }
};

// TODO: don't show file and line if not built with debugging
// TODO: change so it throws a different type
//#define GhAssert(cond, message) do { if (!(cond)) { throw GhException(__FILE__, __LINE__, message); } } while (false)