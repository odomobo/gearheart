#pragma once

#include <array>
#include <cstddef>

#include "TypeTraits.hpp"

template <typename T>
struct Constructor;

template <typename T>
class ConstructorTag
{
    constexpr ConstructorTag() {}

    friend struct Constructor<T>;
};

template <typename T>
struct Constructor
{
    static constexpr ConstructorTag<T> Tag = ConstructorTag<T>();
};

template <typename TBase, typename ... TDerived>
class alignas(gh::max_alignof<TBase, TDerived...>::value) PolyContainer
{
    static_assert(std::is_polymorphic<TBase>::value, "PolyContainer: TBase must be a polymorphic type");
    static_assert(gh::are_all_subclasses<TBase, TDerived...>::value, "PolyContainer: Invalid type parameters; all of TDerived must be derived from TBase");
    static_assert(gh::are_all_trivially_destructible<TBase, TDerived...>::value, "PolyContainer: All of TBase and TDerived must be trivially destructible");

    std::array<std::byte, gh::max_sizeof<TBase, TDerived...>::value> _data;

public:
    PolyContainer(PolyContainer const & other) : _data{other._data} {}
    PolyContainer(PolyContainer && other) noexcept : _data{other._data} {}

    PolyContainer & operator=(PolyContainer const & other)
    {
        _data = other._data;
        return *this;
    }

    PolyContainer & operator=(PolyContainer && other) noexcept
    {
        _data = other._data;
        return *this;
    }

    template<typename T, typename ... TArgs, typename = std::enable_if_t<gh::is_one_of<T, TDerived...>::value> >
    PolyContainer(ConstructorTag<T> t, TArgs ... args)
    {
        new (&_data[0]) T(args...);
    }

    TBase * operator->()
    {
        return reinterpret_cast<TBase *>(&_data[0]);
    }

    TBase const * operator->() const
    {
        return reinterpret_cast<TBase const *>(&_data[0]);
    }

    TBase & operator*()
    {
        TBase * tmpData = reinterpret_cast<TBase *>(&_data[0]);
        return *tmpData;
    }

    TBase const & operator*() const
    {
        TBase const * tmpData = reinterpret_cast<TBase const *>(&_data[0]);
        return *tmpData;
    }

    ~PolyContainer() {}
};