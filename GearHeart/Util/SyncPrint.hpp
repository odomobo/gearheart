#pragma once

#include "fmt/format.h"
#include <cstdio>
#include <mutex>

template <typename ... Args>
void SyncPrintln(Args ... args)
{
    extern std::mutex PrintMutex;

    std::lock_guard<std::mutex> guard{ PrintMutex };
    // print uses fwrite, so we need to use c stdio
    fmt::print(args...);
    std::putchar('\n');
    std::fflush(stdout);
}

template <typename ... Args>
void SyncPrint(Args ... args)
{
    extern std::mutex PrintMutex;

    std::lock_guard<std::mutex> guard{ PrintMutex };
    // print uses fwrite, so we need to use c stdio
    fmt::print(args...);
    std::fflush(stdout);
}
