#pragma once

#include "Util.hpp"
#include "BaseTypes.hpp"

// forward declarations
#include "Core/DebuggingHooksBase.hpp"
#include "Core/Constants.hpp"

#include "Core/ProtocolInterface.hpp"

#include "Core/Timing.hpp"
#include "Core/Info.hpp"
#include "Core/PvTable.hpp"

#include "Core/CastlingRights.hpp"
#include "Core/ZobristHash.hpp"
#include "Core/History.hpp"
#include "Core/Repetitions.hpp"
#include "Core/IncrementalData.hpp"

#include "Core/PieceLists.hpp"

#include "Core/Config.hpp"
#include "Core/Board.hpp"

// moves depend on board
#include "Core/MoveBase.hpp"
#include "Core/MoveNormal.hpp"
#include "Core/MoveCastling.hpp"
#include "Core/MoveEnPassant.hpp"
#include "Core/MovePromotion.hpp"
#include "Core/MoveContainer.hpp"

#include "Core/OrderedMove.hpp"

#include "Core/PerformMoveGuard.hpp"

// transposition table depends on moves
#include "Core/TranspositionTable.hpp"