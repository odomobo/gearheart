#pragma once

#include <boost/container/small_vector.hpp>

#include "../Engine.hpp"
#include "Parse.hpp"

void ParseMoves(Board & board, std::vector<std::string> const & movesStr);