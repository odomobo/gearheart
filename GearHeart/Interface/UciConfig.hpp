#pragma once

#include <string>

#include "../Engine.hpp"

#include "UciOptions.hpp"


class UciConfig : public Config
{
    std::vector<std::unique_ptr<BaseOption>> _options;

public:
    // This needs to stay in sync with _Config's fields
    UciConfig()
    {
        _options.push_back(std::make_unique<SpinOption>(_TimeBufferMillis, "TimeBufferMillis", 50, 0, 1000));
        _options.push_back(std::make_unique<CheckBoxOption>(_Ponder, "Ponder", false));
        _options.push_back(std::make_unique<SpinOption>(_HashMb, "Hash", 64, 0, 999999));
        _options.push_back(std::make_unique<SpinOption>(_Contempt, "Contempt", 5, -100, 100));
        // TODO: more options as desired, like MultiPV, Threads, ClearHash (button), etc.
    }

    std::vector<std::unique_ptr<BaseOption>> const & GetOptions() const
    {
        return _options;
    }

    std::vector<std::unique_ptr<BaseOption>> & GetOptions()
    {
        return _options;
    }
};