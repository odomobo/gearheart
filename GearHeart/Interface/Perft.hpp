#pragma once

#include "../Engine.hpp"

uint64_t GetPerft(Board & board, int depth);
void Perft(Board & board, int depth);
void Divide(Board & board, int depth);