#pragma once

#include <string>

#include "../Engine.hpp"

// Accepts upper or lowercase strings
PieceType ParsePieceType(std::string const & pieceType);
Piece ParsePiece(std::string const & piece);
Coord ParseCoord(std::string const & coord);
Color ParseColor(std::string const & color);
SimpleMove ParseSimpleMove(std::string const & simpleMove);
CastlingRights ParseCastlingRights(std::string const & castlingInfo);