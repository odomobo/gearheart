#pragma once

#include <string>
#include <boost/algorithm/string.hpp>

#include "../Engine.hpp"

class BaseOption
{
protected:
    std::string const _name;

public:
    BaseOption(std::string const & name) : _name{name}
    {}

    virtual ~BaseOption() = default;

    virtual void SendOption() const = 0;
    virtual void SetOption(std::string const & str) = 0;

    std::string GetName() const
    {
        return _name;
    }

    bool IsName(std::string const & str) const
    {
        return str == _name;
    }
};

class SpinOption : public BaseOption
{
    int & _ref;
    int const _initial;
    int const _min;
    int const _max;

public:
    SpinOption(int & ref, std::string const & name, int initial, int min, int max)
        : BaseOption{name}, _ref{ref}, _initial{initial}, _min{min}, _max{max}
    {
        _ref = initial;
    }

    void SendOption() const override
    {
        SyncPrintln("option name {0} type spin default {1} min {2} max {3}", _name, _initial, _min, _max);
    }

    void SetOption(std::string const & str) override
    {
        _ref = std::stoi(str);
    }
};

class CheckBoxOption : public BaseOption
{
    bool & _ref;
    bool const _initial;

    static std::string EncodeBool(bool b)
    {
        return b ? "true" : "false";
    }

    static bool DecodeBool(std::string str)
    {
        if (str == "true")
            return true;

        if (str == "false")
            return false;

        throw GhException{"Bad boolean value: {0}", str};
    }

public:
    CheckBoxOption(bool & ref, std::string const & name, bool initial)
        : BaseOption{name}, _ref{ref}, _initial{initial}
    {
        _ref = initial;
    }

    void SendOption() const override
    {
        SyncPrintln("option name {0} type check default {1}", _name, EncodeBool(_initial));
    }

    void SetOption(std::string const & str) override
    {
        _ref = DecodeBool(str);
    }
};

class StringOption : public BaseOption
{
    std::string & _ref;
    std::string const _initial;

    static std::string EncodeStr(std::string const & str)
    {
        return (str == "") ? "<empty>" : str;
    }

    static std::string DecodeStr(std::string const & str)
    {
        return (str == "<empty>") ? "" : str;
    }

public:
    StringOption(std::string & ref, std::string const & name, std::string const & initial)
        : BaseOption{name}, _ref{ref}, _initial{DecodeStr(initial)}
    {
        _ref = _initial;
    }

    void SendOption() const override
    {
        SyncPrintln("option {0} type string default {1}", _name, EncodeStr(_initial));
    }

    void SetOption(std::string const & str) override
    {
        _ref = DecodeStr(str);
    }
};
