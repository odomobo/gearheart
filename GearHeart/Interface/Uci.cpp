#include <string>

#include "../Interface.hpp"

void Uci::UciIdentification(std::map<std::string, std::vector<std::string>> const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();

    int cpuBits = sizeof(void*)*8;

    SyncPrintln("id name {0} {1} {2}bit", Constants::EngineName, Constants::Version, cpuBits);
    SyncPrintln("id author {0}", Constants::Author);

    for (auto const & option : _uciConfig.GetOptions())
    {
        option->SendOption();
    }

    SyncPrintln("uciok");
}

void Uci::Debug(std::map<std::string, std::vector<std::string>> const & args)
{
    // we don't really need to stop and synchronize for this
    if (args.count("off") > 0)
    {
        _engine._Debug = false;
    }
    else if (args.count("on") > 0)
    {
        _engine._Debug = true;
    }
    else
    {
        throw GhException{"Could not interpret debug command"};
    }
}

void Uci::SetOption(std::map<std::string, std::vector<std::string>> const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    std::string const & name = args.at("name").at(0);

    auto const & valueVec = args.at("value");
    std::string value = boost::algorithm::join(valueVec, " ");

    for (auto & option : _uciConfig.GetOptions())
    {
        if (name == option->GetName())
        {
            option->SetOption(value);
            _engine.ConfigUpdated();
            return;
        }
    }
    SyncPrintln("No such option: {0}", name);
}

void Uci::Register(std::map<std::string, std::vector<std::string>> const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    SyncPrintln("registration ok");
    SyncPrintln("info string No registration required");
}

void Uci::UciNewGame(std::map<std::string, std::vector<std::string>> const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    // Clear the TT hash; maybe other things in the future.
    _engine._TranspositionTable.Clear();
}

void Uci::Position(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    // Potential optimization: check if there's a partial match between the given position and the current position
    if (args.count("startpos") > 0)
    {
        ParseFen(_engine._Board, { "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR", "w", "KQkq", "-", "0", "1" });
    }
    else if (args.count("fen") > 0)
    {
        ParseFen(_engine._Board, args.at("fen"));
    }
    else
    {
        throw GhException{"Expected either \"fen\" or \"startpos\""};
    }
    
    if (args.count("moves") > 0)
    {
        ParseMoves(_engine._Board, args.at("moves"));
    }

    // tell the engine which side is to move
    _engine.SetEvaluatingForSide(_engine._Board.GetToMove());

    // TODO: if debugging is on, then print position evaluation parameters and whatnot
}

void Uci::Display(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    DumpInfo(_engine._Board);
}

void Uci::Perft(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    int depth = std::stoi(args.at("").at(0));
    ::Perft(_engine._Board, depth);
}

void Uci::Divide(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    int depth = std::stoi(args.at("").at(0));
    ::Divide(_engine._Board, depth);
}

void Uci::CheckAttacked(std::map<std::string, std::vector<std::string>> const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    DumpAttackedSquaresInfo(_engine._Board);
}

void Uci::Go(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    if (!_engine._Board.HasBothKings())
        throw GhException{"Invalid board state"};

    _engine._Timing = Timing{};
    _engine._Info = Info{};

    // calculate time to spend thinking
    std::optional<Duration> durationLeftOnClock{};
    if (_engine._Board.GetToMove() == White)
    {
        if (args.count("wtime") > 0)
        {
            durationLeftOnClock = std::chrono::milliseconds{ std::stoi(args.at("wtime")[0]) };
        }
    }
    else
    {
        if (args.count("btime") > 0)
        {
            durationLeftOnClock = std::chrono::milliseconds{ std::stoi(args.at("btime")[0]) };
        }
    }

    if (durationLeftOnClock.has_value())
    {
        int movesToGo;
        if (args.count("movestogo") > 0)
            movesToGo = std::stoi(args.at("movestogo")[0]);
        else
            movesToGo = 30;

        if (movesToGo <= 0)
            throw GhException{"Bad count of moves left before next time interval: {0}", movesToGo};

        Duration searchDuration = *durationLeftOnClock / movesToGo;
        _engine._Timing.SearchDuration = searchDuration - std::chrono::milliseconds{_engine._Config._TimeBufferMillis};
        _engine._Timing.IsSearchTimeExact = false;
    }

    if (args.count("movetime") > 0)
    {
        Duration searchDuration = std::chrono::milliseconds{ std::stoi(args.at("movetime")[0]) };
        _engine._Timing.SearchDuration = searchDuration - std::chrono::milliseconds{_engine._Config._TimeBufferMillis};
        _engine._Timing.IsSearchTimeExact = true;
    }

    if (args.count("depth") > 0)
        _engine._Timing.MaxSearchDepth = std::stoi(args.at("depth")[0]);

    if (args.count("nodes") > 0)
        _engine._Timing.MaxSearchNodes = std::stoi(args.at("nodes")[0]);

    if (args.count("mate") > 0)
        _engine._Timing.SearchForMateIn = std::stoi(args.at("mate")[0]);

    if (args.count("infinite") > 0)
        _engine._Timing.Infinite = true;

    bool ponder = args.count("ponder") > 0;
    if (!ponder)
        _engine._Timing.ClockStartTime = Clock::now();

    _engine._CommandProcessor.InterfaceSearch(ponder);
}

void Uci::PonderHit(std::map<std::string, std::vector<std::string> > const & args)
{
    if (!_engine._CommandProcessor.IsPondering())
        return;

    _engine._Timing.ClockStartTime = Clock::now();
    _engine._CommandProcessor.InterfacePonderHit();
}

void Uci::Stop(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
}

void Uci::IsReady(std::map<std::string, std::vector<std::string> > const & args)
{
    _engine._CommandProcessor.InterfaceStopAndSynchronize();
    SyncPrintln("readyok");
}
