#pragma once

#include <string>

#include "../Engine.hpp"

void ParseFen(Board & board, std::vector<std::string> const & fen);
std::string GetFen(Board const & board);