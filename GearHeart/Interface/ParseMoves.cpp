#include "../Interface.hpp"

void ParseMoves(Board & board, std::vector<std::string> const & movesStr)
{
    boost::container::small_vector<MoveContainer, 64> possibleMoves;
    for (auto & moveStr : movesStr)
    {
        bool foundMove = false;
        SimpleMove move = ParseSimpleMove(moveStr);

        possibleMoves.clear();
        GenerateLegalMoves<SearchType::Normal>(possibleMoves, board);

        for (auto & possibleMove : possibleMoves)
        {
            if (*possibleMove == move)
            {
                possibleMove->PerformMove<IsSearching::Moving>(board);
                foundMove = true;
                break;
            }
        }

        if (!foundMove)
            throw GhException{"Could not perform move {0}", moveStr};
    }
}