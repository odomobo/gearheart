#include <ctype.h>
#include <boost/algorithm/string.hpp>

#include "../Interface.hpp"

// Accepts upper or lowercase strings
PieceType ParsePieceType(std::string const & pieceType)
{
    std::string pt = boost::to_lower_copy(pieceType);
    if (pt == "p")
        return Pawn;
    if (pt == "n")
        return Knight;
    if (pt == "b")
        return Bishop;
    if (pt == "r")
        return Rook;
    if (pt == "q")
        return Queen;
    if (pt == "k")
        return King;

    throw GhException{"Could not parse piece type \"{0}\"", pieceType};
}

Piece ParsePiece(std::string const & piece)
{
    if (std::size(piece) != 1)
        throw GhException{"Could not parse piece \"{0}\": should only be length of 1", piece};

    PieceType const pt = ParsePieceType(piece);

    if (isupper(piece[0]))
        return White + pt;
    else
        return Black + pt;
}

Coord ParseCoord(std::string const& coord)
{
    if (coord == "-")
        return CoordInvalid;

    if (std::size(coord) != 2)
        throw GhException{"Could not parse coord \"{0}\": should either be \"-\" (for invalid coord), or needs to be a length of 2", coord};

    if (coord[0] < 'a' || coord[0] > 'h' || coord[1] < '1' || coord[1] > '8')
        throw GhException{"Could not parse coord \"{0}\": needs to be something like \"e4\"", coord};

    return Coord{coord[0]-'a', coord[1]-'1'};
}

Color ParseColor(std::string const& color)
{
    if (color == "w")
        return White;
    if (color == "b")
        return Black;

    throw GhException{"Could not parse \"{0}\" as a valid color", color};
}

SimpleMove ParseSimpleMove(std::string const& simpleMove)
{
    if (std::size(simpleMove) < 4 || std::size(simpleMove) > 5)
        throw GhException{"Move \"{0}\" could not be parsed", simpleMove};

    Coord srcCoord = ParseCoord(simpleMove.substr(0, 2));
    Coord dstCoord = ParseCoord(simpleMove.substr(2, 2));

    std::string promotionStr = simpleMove.substr(4, 1);
    PieceType promotionType = PieceTypeNone;
    if (promotionStr != "")
        promotionType = ParsePieceType(promotionStr);
    
    return SimpleMove{srcCoord, dstCoord, promotionType};
}

CastlingRights ParseCastlingRights(std::string const & castlingInfo)
{
    // CastlingRights constructor defaults to all false
    CastlingRights castlingRights;

    if (castlingInfo == "-")
        return castlingRights;

    for (char c : castlingInfo)
    {
        Piece const piece = ParsePiece(std::string{c});
        castlingRights.Set(piece, true);
    }
    return castlingRights;
}
