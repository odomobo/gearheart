#pragma once

#include "../Engine.hpp"
#include "DumpInfo.hpp"

inline void DumpAttackedSquaresInfo(Board & board)
{
    DumpInfo(board);

    Board whiteAttackedSquares{};
    Board blackAttackedSquares{};

    for (int rank = 0; rank < 8; rank++)
    {
        for (int file = 0; file < 8; file++)
        {
            Coord coord{file, rank};
            if (MoveGeneration::CoordIsAttacked(board, coord, White))
                whiteAttackedSquares.AddPiece(coord, WhiteKing);

            if (MoveGeneration::CoordIsAttacked(board, coord, Black))
                blackAttackedSquares.AddPiece(coord, BlackKing);

        }
    }

    SyncPrintln("White would be attacked at squares:\n{0}\nBlack would be attacked at squares:\n{1}", 
        GetBoardLayoutString(whiteAttackedSquares), GetBoardLayoutString(blackAttackedSquares));
}