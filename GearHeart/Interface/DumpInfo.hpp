#pragma once

#include <string>

#include "../Core.hpp"

inline std::string GetBoardLayoutString(Board const & board)
{
    std::string ret;
    for (int rank = 7; rank >= 0; rank--)
    {
        for (int file = 0; file < 8; file++)
        {
            ret += fmt::format("{0} ", board.GetPiece(file, rank));
        }
        ret += "\n";
    }
    return ret;
}

inline void DumpInfo(Board & board)
{
    std::string tmp = "\n";
    if (board.GetToMove() == Black)
        tmp += "VVV To Move VVV\n";

    tmp += GetBoardLayoutString(board);

    if (board.GetToMove() == White)
        tmp += "^^^ To Move ^^^\n";

    tmp += "\n";
    tmp += fmt::format("Fen: {0}\n", GetFen(board));
    tmp += fmt::format("Evaluation: {0}\n", Evaluate(board));
    tmp += fmt::format("Zobrist Hash: {0}\n", board.GetZobristHash());
    
    if (board.IsRepetition())
    {
        tmp += "Draw by repetition\n";
    }
    else
    {
        std::vector<MoveContainer> moves;
        GenerateLegalMoves<SearchType::Normal>(moves, board);
        // print all moves
        tmp += fmt::format("Moves({0}):\n", std::size(moves));
        for (auto & move : moves)
        {
            tmp += ToString(move) + " ";
        }
    }

    SyncPrintln(tmp);
}