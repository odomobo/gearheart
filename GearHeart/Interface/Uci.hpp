#pragma once

#include <string>
#include <iostream>
#include <thread>

#include <boost/algorithm/string.hpp>

#include "../Engine.hpp"

#include "UciConfig.hpp"
#include "UciCommandProcessor.hpp"

class Uci : public ProtocolInterface
{
    UciConfig _uciConfig;
    TranspositionTable _transpositionTable;
    Engine _engine;
    UciCommandProcessor _commandProcessor;

    void SendInfoStringImpl(std::string const & message) const override
    {
        SyncPrintln("info string {0}", message);
    }

    void UciIdentification(std::map<std::string, std::vector<std::string> > const & args);
    void Debug(std::map<std::string, std::vector<std::string> > const & args);
    void SetOption(std::map<std::string, std::vector<std::string>> const & args);
    void Register(std::map<std::string, std::vector<std::string> > const & args);
    void UciNewGame(std::map<std::string, std::vector<std::string> > const & args);
    void Position(std::map<std::string, std::vector<std::string> > const & args);
    void Display(std::map<std::string, std::vector<std::string> > const & args);
    void Perft(std::map<std::string, std::vector<std::string> > const & args);
    void Divide(std::map<std::string, std::vector<std::string> > const & args);
    void CheckAttacked(std::map<std::string, std::vector<std::string> > const & args);
    void Go(std::map<std::string, std::vector<std::string> > const & args);
    void PonderHit(std::map<std::string, std::vector<std::string> > const & args);
    void Stop(std::map<std::string, std::vector<std::string> > const & args);
    void IsReady(std::map<std::string, std::vector<std::string> > const & args);

public:
    Uci() :
        _uciConfig{},
        _transpositionTable{_uciConfig._HashMb},
        _engine{static_cast<Config &>(_uciConfig), *this, _transpositionTable},
        _commandProcessor{
            UciCommand{"position", UciCommandKeys{"startpos", "fen", "moves"}, &Uci::Position},
            UciCommand{"uci", UciCommandKeys{}, &Uci::UciIdentification},
            UciCommand{"debug", UciCommandKeys{"on", "off"}, &Uci::Debug},
            UciCommand{"setoption", UciCommandKeys{"name", "value"}, &Uci::SetOption},
            UciCommand{"register", UciCommandKeys{}, &Uci::Register},
            UciCommand{"ucinewgame", UciCommandKeys{}, &Uci::UciNewGame},
            UciCommand{"go", UciCommandKeys{"searchmoves", "ponder", "wtime", "btime", "winc", "binc", "movestogo", "depth", "nodes", "mate", "movetime", "infinite"}, &Uci::Go},
            UciCommand{"ponderhit", UciCommandKeys{}, &Uci::PonderHit},
            UciCommand{"stop", UciCommandKeys{}, &Uci::Stop},
            UciCommand{"isready", UciCommandKeys{}, &Uci::IsReady},
            UciCommand{"d", UciCommandKeys{}, &Uci::Display},
            UciCommand{"perft", UciCommandKeys{}, &Uci::Perft},
            UciCommand{"divide", UciCommandKeys{}, &Uci::Divide},
            UciCommand{"check_attacked", UciCommandKeys{}, &Uci::CheckAttacked},
        }
    {}

    void Loop()
    {
        std::thread engineThread{&Engine::Loop, std::ref(_engine)};
        _commandProcessor.Process(*this, "position", {"startpos"});
        while (true)
        {
            std::string line;
            std::getline(std::cin, line);
            boost::algorithm::trim_right(line);

            std::vector<std::string> splitLine;
            boost::split(splitLine, line, boost::is_any_of(" "), boost::token_compress_on);

            std::string command;
            if (std::size(splitLine) > 0)
                command = splitLine[0];

            if (command == "quit")
            {
                _engine._CommandProcessor.InterfaceStopAndSynchronize();
                _engine._CommandProcessor.InterfaceQuit();
                engineThread.join();
                return;
            }
            else if (command == "" || command[0] == '#')
            {
                continue;
            }
            else if (_commandProcessor.Contains(command))
            {
                std::vector<std::string> const rest(splitLine.begin()+1, splitLine.end());
                _commandProcessor.Process(*this, command, rest);
            }
            else
            {
                SyncPrintln("Unknown command: {0}", command);
            }
        }
    }

    void SendInfo(Score const score) const override
    {
        std::string scoreString;
        if (!score.IsMate())
        {
            scoreString = fmt::format("cp {0}", score.GetCentipawns());
        }
        else
        {
            scoreString = fmt::format("mate {0}", score.MateInMoves());
        }

        auto duration = Clock::now() - _engine._Timing.SearchStartTime;
        int durationMillis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
        int nps = 0;
        if (durationMillis > 0)
        {
            nps = (_engine._Info.NodesVisited*1000)/durationMillis;
        }
        std::vector<std::string> pv;
        for (auto & pvMove : _engine._PvTable.GetBestLine())
        {
            pv.push_back(ToString(pvMove));
        }
        std::string pvString = boost::algorithm::join(pv, " ");

        SyncPrintln(
            "info score {0} nodes {1} time {2} nps {3} depth {4} seldepth {5} pv {6}",
            scoreString,
            _engine._Info.NodesVisited,
            durationMillis,
            nps,
            _engine._Info.Depth,
            _engine._Info.SelDepth,
            pvString
        );
    }

    void SendBestMove(SimpleMove const & move, SimpleMove const & ponderMove = SimpleMoveNull) const override
    {
        if (ponderMove == SimpleMoveNull || !_uciConfig._Ponder)
        {
            SyncPrintln("bestmove {0}", move);
        }
        else
        {
            SyncPrintln("bestmove {0} ponder {1}", move, ponderMove);
        }
    }
};
