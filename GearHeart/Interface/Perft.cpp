
#include <boost/container/small_vector.hpp>

#include "../Interface.hpp"

uint64_t GetPerft(Board & board, int depth)
{
    if (depth <= 0)
        return 1;

    boost::container::small_vector<MoveContainer, 64> moves;
    GenerateLegalMoves<SearchType::Normal>(moves, board);
    if (depth == 1)
        return std::size(moves);

    uint64_t count = 0;
    for (MoveContainer const & move : moves)
    {
        PerformMoveGuard<IsSearching::Moving> guard{board, *move};
        count += GetPerft(board, depth-1);
    }
    return count;
}

void Perft(Board & board, int depth)
{
    if (!board.HasBothKings())
        throw GhException{"Board doesn't have the correct number of kings"};
    
    auto start = Clock::now();
    auto count = GetPerft(board, depth);
    auto end = Clock::now();
    int durationMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    int knps = durationMillis > 0 ? count/durationMillis : 0;

    SyncPrintln("{0} nodes in {1} ms ({2} knps)", count, durationMillis, knps);
}

void Divide(Board & board, int depth)
{
    if (!board.HasBothKings())
        throw GhException{"Board doesn't have the correct number of kings"};

    if (depth < 1)
        throw GhException{"Depth for divide must be at least 1"};

    std::vector<MoveContainer> moves;
    GenerateLegalMoves<SearchType::Normal>(moves, board);
    
    std::map<SimpleMove, uint64_t> results;

    for (MoveContainer const & move : moves)
    {
        PerformMoveGuard<IsSearching::Moving> guard{board, *move};
        results[*move] = GetPerft(board, depth-1);
    }

    for (auto const & [key, val] : results)
    {
        SyncPrintln("{0} {1}", key, val);
    }
}