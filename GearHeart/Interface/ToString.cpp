#include <string>
#include <boost/algorithm/string.hpp>

#include "../Interface.hpp"


std::string ToString(PieceType const type)
{
    if (type == Pawn)
        return "p";
    if (type == Bishop)
        return "b";
    if (type == Knight)
        return "n";
    if (type == Rook)
        return "r";
    if (type == Queen)
        return "q";
    if (type == King)
        return "k";

    throw GhException{"Unknown piece type"};
}

std::string ToString(Color const color)
{
    if (color == Black)
        return "b";
    if (color == White)
        return "w";
    
    throw GhException{"Unknown color"};
}

std::string ToString(CastlingRights const castlingRights)
{
    std::string ret;

    for (Piece piece : {WhiteKing, WhiteQueen, BlackKing, BlackQueen})
    {
        if (castlingRights.CanCastleLike(piece))
        {
            ret += ToString(piece);
        }
    }

    if (std::size(ret) == 0)
        ret += "-";

    return ret;
}

std::string ToString(SimpleMove const & simpleMove)
{
    if (simpleMove == SimpleMoveNull)
        return "0000";

    std::string captureType;
    if (simpleMove._Promotion != PieceTypeNone)
        captureType = ToString(simpleMove._Promotion); // TODO: should this be uppercase?

    return fmt::format("{0}{1}{2}", simpleMove._SrcCoord, simpleMove._DstCoord, captureType);
}

std::string ToString(MoveContainer const & moveContainer)
{
    SimpleMove const & simpleMove = *moveContainer;
    return ToString(simpleMove);
}

std::string ToString(ZobristHash const & zobristHash)
{
    uint64_t hashValue = static_cast<uint64_t>(zobristHash);

    // formats like: 0x01020304ffffffff
    // See: http://fmtlib.net/latest/syntax.html
    return fmt::format("0x{:016x}", hashValue);
}

