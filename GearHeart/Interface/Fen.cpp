#include <algorithm>

#include <boost/algorithm/string.hpp>

#include "../Interface.hpp"

static void ParseBoard(Board & board, std::string const & boardInfo)
{
    
    std::vector<std::string> ranks;
    boost::split(ranks, boardInfo, boost::is_any_of("/"));
    if (std::size(ranks) != 8)
        throw GhException{"Fen board is not in a valid format"};

    // ranks come in reversed order
    std::reverse(std::begin(ranks), std::end(ranks));

    for (int rankIx = 0; rankIx < 8; rankIx++)
    {
        std::string const & rank = ranks[rankIx];

        int fileIx = 0;
        for (char c : rank)
        {
            if (c >= '1' && c <= '8')
            {
                int count = c - '0';
                for (; count > 0; count--)
                {
                    if (Coord{fileIx, rankIx}.IsInvalid())
                        throw GhException{"Fen board at rank {0} isn't in a valid format", rankIx+1};

                    fileIx++;
                }
            }
            else
            {
                if (Coord{fileIx, rankIx}.IsInvalid())
                    throw GhException{"Fen board at rank {0} isn't in a valid format", rankIx+1};

                Piece const piece = ParsePiece(std::string{c});
                board.AddPiece(Coord{fileIx, rankIx}, piece);
                fileIx++;
            }
        }
        if (fileIx != 8)
            throw GhException{"Fen board at rank {0} isn't in a valid format", rankIx+1};
    }
}

static void ParseToMove(Board & board, std::string const & toMove)
{
    board.SetToMove(ParseColor(toMove));
}

static void ParseCastling(Board & board, std::string const & castlingInfo)
{
    board.SetCastlingRights(ParseCastlingRights(castlingInfo));
}

static void ParseEnPassant(Board & board, std::string const & enPassant)
{
    board.SetEnPassantCoord(ParseCoord(enPassant));

    // TODO: verify that the en passant square is valid for the current board configuration
}

static void ParseHalfmove(Board & board, std::string const & halfmove)
{
    board.SetHalfmoves(std::stoi(halfmove));
}

static void ParseMoveNumber(Board & board, std::string const & moveNumber)
{
    board.SetMoveNumber(std::stoi(moveNumber));
}

void ParseFen(Board & board, std::vector<std::string> const & fen)
{
    board.Clear();
    std::vector<std::string> tmpFen = fen;

    if (std::size(tmpFen) < 4 || std::size(tmpFen) > 6)
        throw GhException{"Fen string is not in a valid format"};

    if (std::size(tmpFen) == 4)
        tmpFen.push_back("0");

    if (std::size(tmpFen) == 5)
        tmpFen.push_back("1");

    ParseBoard(board, tmpFen[0]);
    ParseToMove(board, tmpFen[1]);
    ParseCastling(board, tmpFen[2]);
    ParseEnPassant(board, tmpFen[3]);
    ParseHalfmove(board, tmpFen[4]);
    ParseMoveNumber(board, tmpFen[5]);
    board.UpdateCastlingRights();
    board.UpdateZobristHash();
    board.AddRepetition<IsSearching::Moving>();
}

static std::string GetFenBoard(Board const & board)
{
    std::vector<std::string> ranks;

    for (int rankIx = 0; rankIx < 8; rankIx++)
    {
        std::string rank;
        int blankSpaces = 0;
        for (int fileIx = 0; fileIx < 8; fileIx++)
        {
            Piece piece = board.GetPiece(fileIx, rankIx);
            if (piece == Space)
            {
                blankSpaces++;
            }
            else
            {
                if (blankSpaces > 0)
                {
                    rank += ('0' + blankSpaces);
                    blankSpaces = 0;
                }
                rank += ToString(piece);
            }
        }
        if (blankSpaces > 0)
        {
            rank += ('0' + blankSpaces);
            blankSpaces = 0;
        }

        ranks.push_back(rank);
    }

    // ranks must be provided in reverse order
    std::reverse(std::begin(ranks), std::end(ranks));

    return boost::algorithm::join(ranks, "/");
}

std::string GetFen(Board const & board)
{
    return fmt::format(
        "{0} {1} {2} {3} {4} {5}",
        GetFenBoard(board),
        board.GetToMove(),
        board.GetCastlingRights(),
        board.GetEnPassantCoord(),
        board.GetHalfmoves(),
        board.GetMoveNumber()
    );
}
