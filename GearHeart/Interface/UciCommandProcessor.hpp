#pragma once
#include <string>
#include <set>
#include <map>
#include <vector>

class Uci;

typedef std::set<std::string> UciCommandKeys;
typedef std::function<void(Uci * uci, std::map<std::string, std::vector<std::string> > const & args)> UciCommandHandler;

struct UciCommand
{
    std::string _Command;
    UciCommandKeys _Keys;
    UciCommandHandler _Handler;
};

class UciCommandProcessor
{
    std::map<std::string, UciCommand> _uciCommands;
public:
    UciCommandProcessor(std::initializer_list<UciCommand> iList)
    {
        for (UciCommand const & uciCommand : iList)
        {
            _uciCommands[uciCommand._Command] = uciCommand;
        }
    }

    bool Contains(std::string const command) const
    {
        return _uciCommands.count(command) > 0;
    }

    void Process(Uci & uci, std::string const commandStr, std::vector<std::string> const args)
    {
        UciCommand & command = _uciCommands.at(commandStr);

        std::string key = "";
        std::map<std::string, std::vector<std::string> > argsMap;

        for (std::string const & arg : args)
        {
            if (command._Keys.count(arg) > 0)
            {
                key = arg;
                argsMap[key]; // this ensures that the key gets created even if it has no values
            }
            else
            {
                argsMap[key].push_back(arg);
            }
        }

        try
        {
            command._Handler(&uci, argsMap);
        }
        catch (std::exception const & e)
        {
            SyncPrintln("Exception: {0}", e.what());
        }
    }
};