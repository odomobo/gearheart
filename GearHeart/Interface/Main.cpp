#include <string_view>

#include "../Interface.hpp"

// if the Uci object lives in bss, the compiler can see that only one instance of engine and board ever exist, so it can optimize away those pointers during method calls
Uci uci;

int Main(std::string_view command, gsl::span<std::string_view> args)
{
    // TODO: implement command-line argument processing, if desired

    uci.Loop();
    return 0;
}

int main(int argc, char *argv[])
{
    std::vector<std::string_view> args{};
    for (int i = 1; i < argc; i++)
    {
        args.emplace_back(argv[i]);
    }
    return Main(argv[0], args);
}