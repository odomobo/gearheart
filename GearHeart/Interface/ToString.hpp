#pragma once

#include <string>

#include <boost/algorithm/string.hpp>

#include "../Engine.hpp"

std::string ToString(PieceType const type);
inline std::ostream & operator<<(std::ostream& os, PieceType const type)
{
    return os << ToString(type);
}

std::string ToString(Color const color);
inline std::ostream & operator<<(std::ostream& os, Color const color)
{
    return os << ToString(color);
}

std::string ToString(CastlingRights const castlingRights);
inline std::ostream & operator<<(std::ostream& os, CastlingRights const castlingRights)
{
    return os << ToString(castlingRights);
}

std::string ToString(SimpleMove const & simpleMove);
inline std::ostream & operator<<(std::ostream& os, SimpleMove const & simpleMove)
{
    return os << ToString(simpleMove);
}

std::string ToString(MoveContainer const & moveContainer);
inline std::ostream & operator<<(std::ostream& os, MoveContainer const & moveContainer)
{
    return os << ToString(moveContainer);
}

std::string ToString(ZobristHash const & zobristHash);
inline std::ostream & operator<<(std::ostream& os, ZobristHash const & zobristHash)
{
    return os << ToString(zobristHash);
}

inline std::string ToString(MoveBase const & move)
{
    return ToString(static_cast<SimpleMove const &>(move));
}
inline std::ostream & operator<<(std::ostream& os, MoveBase const & move)
{
    return os << ToString(move);
}

template <typename T>
std::string ToString(PieceBase<T> const piece)
{
    if (piece == Space)
        return ".";

    std::string ret = ToString(piece.GetType());

    if (piece.GetColor() == White)
        boost::to_upper(ret);

    return ret;
}

template <typename T>
std::ostream & operator<<(std::ostream& os, PieceBase<T> const piece)
{
    return os << ToString(piece);
}


template <typename T>
std::string ToString(CoordBase<T> const coord)
{
    if (coord.IsInvalid())
        return "-";

    return std::string{
        static_cast<char>('a' + coord.GetFile()),
        static_cast<char>('1' + coord.GetRank())
    };
}

template <typename T>
std::ostream & operator<<(std::ostream& os, CoordBase<T> const coord)
{
    return os << ToString(coord);
}

template <typename T>
std::ostream & operator<<(std::ostream& os, ScoreBase<T> const score)
{
    return os << static_cast<T>(score);
}

inline std::ostream & operator<<(std::ostream& os, WhiteScore const score)
{
    return os << static_cast<int_t>(score);
}