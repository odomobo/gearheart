#pragma once

#include "Typedefs.hpp"

class Offset;

template <typename TBacking>
class CoordBase
{
    TBacking _data;

    static constexpr TBacking FileMask = 0b00001111;
    static constexpr int RankShift = 4;

public:
    constexpr CoordBase() : _data{0x88} {}

    constexpr CoordBase(int file, int rank) : _data{static_cast<TBacking>( (rank << RankShift) + file )} {}

    constexpr explicit CoordBase(TBacking data) : _data{data} {}

    // Any CoordBase can implicitly convert to any other CoordBase type
    template <typename TOther>
    constexpr operator CoordBase<TOther> () const
    {
        return CoordBase<TOther>{static_cast<TOther>(_data)};
    }

    constexpr explicit operator TBacking () const
    {
        return _data;
    }

    constexpr int GetRank() const
    {
        return static_cast<int>(_data >> RankShift);
    }

    constexpr int GetFile() const
    {
        return static_cast<int>(_data & FileMask);
    }

    constexpr size_t GetArrayIndex() const
    {
        return _data;
    }

    // Gets the index into a 64-element array, starting from a1 to h1 (rank-major order)
    constexpr size_t Get64ArrayIndex() const
    {
        return GetRank()*8 + GetFile();
    }

    constexpr bool operator==(CoordBase const other) const
    {
        return _data == other._data;
    }

    constexpr bool operator!=(CoordBase const other) const
    {
        return _data != other._data;
    }

    constexpr bool operator<(CoordBase const other) const
    {
        if (GetFile() != other.GetFile())
            return GetFile() < other.GetFile();

        return GetRank() < other.GetRank();
    }

    constexpr bool IsInvalid() const
    {
        return _data & 0x88;
    }

    constexpr bool IsValid() const
    {
        return !IsInvalid();
    }

    constexpr CoordBase GetFlipped() const
    {
        return CoordBase{GetFile(), 7 - GetRank()};
    }
    
    friend class Offset;
};

typedef CoordBase<_coord_t> Coord;
typedef CoordBase<_tiny_coord_t> TinyCoord;


class Offset
{
    _coord_t _data;

public:
    constexpr Offset() : _data{} {}

    constexpr Offset(int file, int rank) : _data{static_cast<_coord_t>( (rank << Coord::RankShift) + file )} {}

    constexpr explicit Offset(_coord_t data) : _data{data} {}

    constexpr explicit operator _coord_t () const
    {
        return _data;
    }

    constexpr int GetRank() const
    {
        return static_cast<int>(_data >> Coord::RankShift);
    }

    constexpr int GetFile() const
    {
        return static_cast<int>(_data & Coord::FileMask);
    }

    constexpr bool operator==(Offset const other) const
    {
        return _data == other._data;
    }

    constexpr bool operator!=(Offset const other) const
    {
        return _data != other._data;
    }

    constexpr Offset operator+(Offset const other) const
    {
        return Offset{_data + other._data};
    }

    constexpr Offset operator*(int mult) const
    {
        return Offset{_data * mult};
    }

    constexpr Offset operator/(int div) const
    {
        return Offset{_data / div};
    }
};

constexpr Coord operator+(Coord const coord, Offset const offset)
{
    _coord_t coordVal = static_cast<_coord_t>(coord);
    _coord_t offsetVal = static_cast<_coord_t>(offset);
    return Coord{coordVal + offsetVal};
}

constexpr Coord operator-(Coord const coord, Offset const offset)
{
    _coord_t coordVal = static_cast<_coord_t>(coord);
    _coord_t offsetVal = static_cast<_coord_t>(offset);
    return Coord{coordVal - offsetVal};
}

constexpr Offset operator-(Coord const coord1, Coord const coord2)
{
    _coord_t coord1Val = static_cast<_coord_t>(coord1);
    _coord_t coord2Val = static_cast<_coord_t>(coord2);
    return Offset{coord1Val - coord2Val};
}

constexpr Coord const CoordInvalid{8, 8};

// Generated using the following python 3 snippet:
/*
for file in range(8):
  for rank in range(8):
    print("constexpr Coord const {}{} {{ {}, {} }};".format(chr(file+97),rank+1,file,rank))
*/

constexpr Coord const a1 { 0, 0 };
constexpr Coord const a2 { 0, 1 };
constexpr Coord const a3 { 0, 2 };
constexpr Coord const a4 { 0, 3 };
constexpr Coord const a5 { 0, 4 };
constexpr Coord const a6 { 0, 5 };
constexpr Coord const a7 { 0, 6 };
constexpr Coord const a8 { 0, 7 };
constexpr Coord const b1 { 1, 0 };
constexpr Coord const b2 { 1, 1 };
constexpr Coord const b3 { 1, 2 };
constexpr Coord const b4 { 1, 3 };
constexpr Coord const b5 { 1, 4 };
constexpr Coord const b6 { 1, 5 };
constexpr Coord const b7 { 1, 6 };
constexpr Coord const b8 { 1, 7 };
constexpr Coord const c1 { 2, 0 };
constexpr Coord const c2 { 2, 1 };
constexpr Coord const c3 { 2, 2 };
constexpr Coord const c4 { 2, 3 };
constexpr Coord const c5 { 2, 4 };
constexpr Coord const c6 { 2, 5 };
constexpr Coord const c7 { 2, 6 };
constexpr Coord const c8 { 2, 7 };
constexpr Coord const d1 { 3, 0 };
constexpr Coord const d2 { 3, 1 };
constexpr Coord const d3 { 3, 2 };
constexpr Coord const d4 { 3, 3 };
constexpr Coord const d5 { 3, 4 };
constexpr Coord const d6 { 3, 5 };
constexpr Coord const d7 { 3, 6 };
constexpr Coord const d8 { 3, 7 };
constexpr Coord const e1 { 4, 0 };
constexpr Coord const e2 { 4, 1 };
constexpr Coord const e3 { 4, 2 };
constexpr Coord const e4 { 4, 3 };
constexpr Coord const e5 { 4, 4 };
constexpr Coord const e6 { 4, 5 };
constexpr Coord const e7 { 4, 6 };
constexpr Coord const e8 { 4, 7 };
constexpr Coord const f1 { 5, 0 };
constexpr Coord const f2 { 5, 1 };
constexpr Coord const f3 { 5, 2 };
constexpr Coord const f4 { 5, 3 };
constexpr Coord const f5 { 5, 4 };
constexpr Coord const f6 { 5, 5 };
constexpr Coord const f7 { 5, 6 };
constexpr Coord const f8 { 5, 7 };
constexpr Coord const g1 { 6, 0 };
constexpr Coord const g2 { 6, 1 };
constexpr Coord const g3 { 6, 2 };
constexpr Coord const g4 { 6, 3 };
constexpr Coord const g5 { 6, 4 };
constexpr Coord const g6 { 6, 5 };
constexpr Coord const g7 { 6, 6 };
constexpr Coord const g8 { 6, 7 };
constexpr Coord const h1 { 7, 0 };
constexpr Coord const h2 { 7, 1 };
constexpr Coord const h3 { 7, 2 };
constexpr Coord const h4 { 7, 3 };
constexpr Coord const h5 { 7, 4 };
constexpr Coord const h6 { 7, 5 };
constexpr Coord const h7 { 7, 6 };
constexpr Coord const h8 { 7, 7 };
