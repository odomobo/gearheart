#pragma once

#include "Typedefs.hpp"

class Color
{
    _piece_t _data;

    constexpr static _piece_t const Black    {0b00000000};
    constexpr static _piece_t const White    {0b00001000};
    constexpr static _piece_t const ColorMask{0b00001000};
    constexpr static int const ColorShift{3};

public:
    constexpr explicit Color(_piece_t data) : _data{data} {}

    constexpr explicit operator _piece_t() const
    {
        return _data;
    }

    constexpr bool operator==(Color const other) const
    {
        return _data == other._data;
    }

    constexpr bool operator!=(Color const other) const
    {
        return _data != other._data;
    }

    constexpr Color Other() const
    {
        return Color{_data ^ ColorMask};
    }

    constexpr Color operator~() const
    {
        return Other();
    }

    constexpr size_t GetIndex() const
    {
        return _data >> ColorShift;
    }
};

constexpr Color const Black{0b00000000};
constexpr Color const White{0b00001000};

static constexpr std::array<Color, 2> const Colors{Black, White};