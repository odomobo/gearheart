#pragma once

#include "../Util.hpp"
#include "Typedefs.hpp"
#include "Color.hpp"

// forward declaration
class WhiteScore;

template <typename TBacking>
class ScoreBase
{
    TBacking _score;

    constexpr static TBacking const MaxNonMate{30000}; // >300 pawns indicates mate
    constexpr static TBacking const MateInZero{32000}; // 32767 is the max we could use and still store in int16_t

public:
    constexpr ScoreBase() : _score{0} {}

    constexpr ScoreBase(TBacking score) : _score{score} {}

    // Any ScoreBase can implicitly convert to any other ScoreBase type
    template <typename TOther>
    constexpr operator ScoreBase<TOther>() const
    {
        return ScoreBase<TOther>{ static_cast<TOther>(_score) };
    }

    constexpr explicit operator TBacking() const
    {
        return _score;
    }

    // the implementation has to be after the WhiteScore is declared, but we still want this to be in the header in case we're compiling without LTO
    WhiteScore ToWhiteScore(Color const color) const;

    constexpr bool operator==(ScoreBase const other) const
    {
        return _score == other._score;
    }

    constexpr bool operator!=(ScoreBase const other) const
    {
        return _score != other._score;
    }

    constexpr bool operator<(ScoreBase const other) const
    {
        return _score < other._score;
    }

    constexpr bool operator<=(ScoreBase const other) const
    {
        return _score <= other._score;
    }

    constexpr bool operator>(ScoreBase const other) const
    {
        return _score > other._score;
    }

    constexpr bool operator>=(ScoreBase const other) const
    {
        return _score >= other._score;
    }
    
    // basic math operators
    constexpr ScoreBase operator-() const
    {
        return ScoreBase{-_score};
    }

    constexpr ScoreBase operator+(ScoreBase const other) const
    {
        return ScoreBase{_score + other._score};
    }

    constexpr ScoreBase operator-(ScoreBase const other) const
    {
        return ScoreBase{_score - other._score};
    }

    constexpr ScoreBase operator*(int mult) const
    {
        return ScoreBase{_score * mult};
    }

    constexpr ScoreBase operator*(size_t mult) const
    {
        return ScoreBase{_score * static_cast<TBacking>(mult)};
    }

    constexpr ScoreBase operator/(int div) const
    {
        return ScoreBase{_score / div};
    }

    constexpr ScoreBase operator/(size_t div) const
    {
        return ScoreBase{_score / static_cast<TBacking>(div)};
    }

    // compound assignment operators
    ScoreBase & operator+=(ScoreBase const rhs)
    {

        _score += rhs._score;
        return *this;
    }

    ScoreBase & operator-=(ScoreBase const rhs)
    {

        _score -= rhs._score;
        return *this;
    }

    // This is useful if we ever want to change our representation to something else
    constexpr int GetCentipawns() const
    {
        return static_cast<int>(_score);
    }

    constexpr int GetDecipawns() const
    {
        return static_cast<int>(_score/10);
    }

    constexpr bool IsMate() const
    {
        return std::abs(_score) > MaxNonMate;
    }

    // Returns plies, not moves.
    // If we're currently mated, then it returns 0
    constexpr int MateIn() const
    {
        return static_cast<int>(MateInZero - std::abs(_score));
    }

    constexpr int MateInMoves() const
    {
        int mateInMoves = (MateIn() + 1)/2;
        // even means we're going to be mated
        if (MateIn() % 2 == 0)
        {
            return -mateInMoves;
        }
        else
        {
            return mateInMoves;
        }
    }

    constexpr static ScoreBase CreateMateIn(int plies)
    {
        return ScoreBase{ MateInZero - plies };
    }

    constexpr static ScoreBase CreateMateIn(int plies, Color const color)
    {
        TBacking whiteScore = MateInZero - plies;
        if (color == White)
            return ScoreBase{ whiteScore };
        if (color == Black)
            return ScoreBase{ -whiteScore };

        Unreachable();
    }

    constexpr ScoreBase GetReducedDistanceToMateBy(int pliesToReduce) const
    {
        // don't increase or reduce for non-mate
        if (!IsMate())
            return *this;

        // we reduce distance to mate by increasing the absolute value -- the greater a positive value, the better (and closer we are to mate)
        if (_score < 0)
            return ScoreBase{ static_cast<TBacking>(_score - pliesToReduce) };
        else
            return ScoreBase{ static_cast<TBacking>(_score + pliesToReduce) };
    }

    constexpr ScoreBase GetIncreasedDistanceToMateBy(int pliesToIncrease) const
    {
        // don't increase or reduce for non-mate
        if (!IsMate())
            return *this;

        // we increase distance to mate by decreasing the absolute value -- the smaller a positive value, the worse (and farther we are to mate)
        if (_score < 0)
            return ScoreBase{ static_cast<TBacking>(_score + pliesToIncrease) };
        else
            return ScoreBase{ static_cast<TBacking>(_score - pliesToIncrease) };
    }

    constexpr static ScoreBase Max(ScoreBase s1, ScoreBase s2)
    {
        if (s1 > s2)
            return s1;
        else
            return s2;
    }

    constexpr static ScoreBase Min(ScoreBase s1, ScoreBase s2)
    {
        if (s1 < s2)
            return s1;
        else
            return s2;
    }
};

template <typename TBacking>
constexpr ScoreBase<TBacking> operator*(int mult, ScoreBase<TBacking> const score)
{
    return score*mult;
}


typedef ScoreBase<_score_t> Score;
typedef ScoreBase<_tiny_score_t> TinyScore;

// just needs to be greater than mate-in-zero
constexpr Score const Infinity{32001};


// WhiteScore is for scores which are represented from white's perspective, instead of side-to-move perspective.
// This forces all conversions between the two to be explicit.
class WhiteScore
{
    _score_t _score;

public:
    constexpr WhiteScore() : _score{ 0 } {}

    constexpr WhiteScore(_score_t score) : _score{ score } {}

    constexpr explicit operator _score_t() const
    {
        return _score;
    }

    constexpr Score FromWhiteScore(Color const color) const
    {
        if (color == Black)
            return Score{ -_score };
        if (color == White)
            return Score{ _score };

        Unreachable();
    }

    constexpr bool operator==(WhiteScore const other) const
    {
        return _score == other._score;
    }

    constexpr bool operator!=(WhiteScore const other) const
    {
        return _score != other._score;
    }

    constexpr bool operator<(WhiteScore const other) const
    {
        return _score < other._score;
    }

    constexpr bool operator<=(WhiteScore const other) const
    {
        return _score <= other._score;
    }

    constexpr bool operator>(WhiteScore const other) const
    {
        return _score > other._score;
    }

    constexpr bool operator>=(WhiteScore const other) const
    {
        return _score >= other._score;
    }

    // basic math operators
    constexpr WhiteScore operator-() const
    {
        return WhiteScore{ -_score };
    }

    constexpr WhiteScore operator+(WhiteScore const other) const
    {
        return WhiteScore{ _score + other._score };
    }

    constexpr WhiteScore operator-(WhiteScore const other) const
    {
        return WhiteScore{ _score - other._score };
    }

    constexpr WhiteScore operator*(int mult) const
    {
        return WhiteScore{ _score * mult };
    }

    constexpr WhiteScore operator*(size_t mult) const
    {
        return WhiteScore{ _score * static_cast<_score_t>(mult) };
    }

    constexpr WhiteScore operator/(int div) const
    {
        return WhiteScore{ _score / div };
    }

    constexpr WhiteScore operator/(size_t div) const
    {
        return WhiteScore{ _score / static_cast<_score_t>(div) };
    }

    // compound assignment operators
    WhiteScore & operator+=(WhiteScore const rhs)
    {

        _score += rhs._score;
        return *this;
    }

    WhiteScore & operator-=(WhiteScore const rhs)
    {

        _score -= rhs._score;
        return *this;
    }
};

template <typename TBacking>
WhiteScore ScoreBase<TBacking>::ToWhiteScore(Color const color) const
{
    if (color == Black)
        return WhiteScore{ -_score };
    if (color == White)
        return WhiteScore{ _score };

    Unreachable();
}