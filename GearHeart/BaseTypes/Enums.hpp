#pragma once

#include "../Util.hpp"

enum class IsSearching
{
    Moving,
    Searching,
    TestingMove,
};

enum class SearchType
{
    Normal,
    Quiescence,
};