#pragma once

#include <chrono>

// Why the heck are we using ptrdiff_t and size_t? http://stannum.co.il/blog/5/sized-integer-types-and-signedness
// Long story short, they're the most "native" signed and unsigned integers to the architecture, respectively.
// Because of this, we can expect them to give max performance.

typedef ptrdiff_t int_t;
typedef size_t uint_t;

// these types shouldn't be used directly, if you can avoid it
typedef uint_t _piece_t;
typedef uint8_t _tiny_piece_t;
typedef int_t _coord_t;
typedef int8_t _tiny_coord_t;
typedef int_t _score_t;
typedef int16_t _tiny_score_t;
