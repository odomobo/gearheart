#pragma once

#include "Typedefs.hpp"

class PieceType
{
    _piece_t _data;

public:
    constexpr explicit PieceType(_piece_t data) : _data{data} {}

    constexpr explicit operator _piece_t() const
    {
        return _data;
    }

    constexpr size_t GetIndex() const
    {
        return _data;
    }

    constexpr bool operator==(PieceType const other) const
    {
        return _data == other._data;
    }

    constexpr bool operator!=(PieceType const other) const
    {
        return _data != other._data;
    }
};

constexpr PieceType const PieceTypeNone{0};
constexpr PieceType const Pawn  {1};
constexpr PieceType const Knight{2};
constexpr PieceType const Bishop{3};
constexpr PieceType const Rook  {4};
constexpr PieceType const Queen {5};
constexpr PieceType const King  {6};

// For efficiency, these piece types should come in the order of the piece indexes.
// That way, hopefully, the compiler will optimize away the actual iteration.
static constexpr std::array<PieceType, 6> const PieceTypes{Pawn, Knight, Bishop, Rook, Queen, King};
