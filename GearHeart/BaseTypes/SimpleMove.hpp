#pragma once

#include "../Util.hpp"
#include "Coord.hpp"
#include "PieceType.hpp"

struct SimpleMove
{
    Coord _SrcCoord;
    Coord _DstCoord;
    PieceType _Promotion;

    // TODO: make this be some kind of a null move or something
    SimpleMove() = delete;
    ~SimpleMove() = default;

    SimpleMove(const SimpleMove& other) = default;
    SimpleMove& operator=(const SimpleMove& other) = default;

    constexpr bool operator==(SimpleMove const & other) const
    {
        return _SrcCoord == other._SrcCoord &&
            _DstCoord == other._DstCoord &&
            _Promotion == other._Promotion;
    }

    constexpr bool operator!=(SimpleMove const & other) const
    {
        return !(*this == other);
    }

    // TODO: remove, not needed
    constexpr bool operator<(SimpleMove const & other) const
    {
        if (_SrcCoord != other._SrcCoord)
            return _SrcCoord < other._SrcCoord;

        if (_DstCoord != other._DstCoord)
            return _DstCoord < other._DstCoord;

        auto promotionInt = static_cast<_piece_t>(_Promotion);
        auto otherPromotionInt = static_cast<_piece_t>(other._Promotion);
        return promotionInt < otherPromotionInt;
    }
};

constexpr SimpleMove const SimpleMoveNull{CoordInvalid, CoordInvalid, PieceTypeNone};