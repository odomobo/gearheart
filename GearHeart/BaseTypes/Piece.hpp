#pragma once

#include "Typedefs.hpp"
#include "PieceType.hpp"
#include "Color.hpp"

template <typename TBacking>
class PieceBase
{
    TBacking _data;

    static constexpr TBacking PieceTypeMask = 0b00000111;
    static constexpr TBacking ColorMask     = ~PieceTypeMask;

public:
    constexpr PieceBase() : _data{0} {}

    constexpr explicit PieceBase(TBacking data) : _data{data} {}

    // Any PieceBase can implicitly convert to any other PieceBase type
    template <typename TOther>
    constexpr operator PieceBase<TOther>() const
    {
        return PieceBase<TOther>(_data);
    }

    constexpr explicit operator TBacking () const
    {
        return _data;
    }

    constexpr PieceType GetType() const
    {
        return static_cast<PieceType>(_data & PieceTypeMask);
    }

    constexpr Color GetColor() const
    {
        return static_cast<Color>(_data & ColorMask);
    }

    constexpr bool operator==(PieceBase const other) const
    {
        return _data == other._data;
    }

    constexpr bool operator!=(PieceBase const other) const
    {
        return _data != other._data;
    }
};

typedef PieceBase<_piece_t> Piece;
typedef PieceBase<_tiny_piece_t> TinyPiece;


constexpr Piece operator+(Color const color, PieceType const type)
{
    _piece_t colorVal = static_cast<_piece_t>(color);
    _piece_t typeVal = static_cast<_piece_t>(type);
    return Piece{colorVal | typeVal};
}

constexpr Piece operator+(PieceType const type, Color const color)
{
    return color + type;
}

constexpr Piece const Space{0};

constexpr Piece const WhitePawn   = White + Pawn;
constexpr Piece const WhiteKnight = White + Knight;
constexpr Piece const WhiteBishop = White + Bishop;
constexpr Piece const WhiteRook   = White + Rook;
constexpr Piece const WhiteQueen  = White + Queen;
constexpr Piece const WhiteKing   = White + King;

constexpr Piece const BlackPawn   = Black + Pawn;
constexpr Piece const BlackKnight = Black + Knight;
constexpr Piece const BlackBishop = Black + Bishop;
constexpr Piece const BlackRook   = Black + Rook;
constexpr Piece const BlackQueen  = Black + Queen;
constexpr Piece const BlackKing   = Black + King;
