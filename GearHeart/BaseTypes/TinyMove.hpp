#pragma once

#include "../Util.hpp"
#include "Coord.hpp"
#include "PieceType.hpp"
#include "SimpleMove.hpp"

class TinyMove
{
    TinyCoord _srcCoord;
    TinyCoord _dstCoord;

public:

    TinyMove() = delete;
    ~TinyMove() = default;

    TinyMove(TinyMove const & other) = default;
    TinyMove & operator=(TinyMove const & other) = default;

    constexpr TinyMove(SimpleMove const & other) : _srcCoord{other._SrcCoord}, _dstCoord{other._DstCoord} {}
    constexpr TinyMove(TinyCoord const srcCoord, TinyCoord const dstCoord) : _srcCoord{srcCoord}, _dstCoord{dstCoord} {}

    // This needs to be explicit, because it's not always valid!
    constexpr explicit operator SimpleMove() const
    {
        return SimpleMove{ _srcCoord, _dstCoord, PieceTypeNone };
    }

    constexpr bool operator==(SimpleMove const & other) const
    {
        return _srcCoord == other._SrcCoord &&
            _dstCoord == other._DstCoord &&
            (other._Promotion == PieceTypeNone || other._Promotion == Queen);
    }

    constexpr bool operator!=(SimpleMove const & other) const
    {
        return !(*this == other);
    }
};

constexpr bool operator==(SimpleMove const & m1, TinyMove const & m2)
{
    return m2 == m1;
}

constexpr bool operator!=(SimpleMove const & m1, TinyMove const & m2)
{
    return m2 != m1;
}

constexpr TinyMove const TinyMoveNull{ CoordInvalid, CoordInvalid };