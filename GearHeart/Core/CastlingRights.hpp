#pragma once

#include <array>

#include "../BaseTypes.hpp"

class Board;

class CastlingRights
{
    std::array<bool, 4> _data;

public:
    // Default constructor sets everything to false
    CastlingRights() : _data{false, false, false, false} {}
    
    constexpr bool GetWhiteKing() const
    {
        return _data[0];
    }

    constexpr bool GetWhiteQueen() const
    {
        return _data[1];
    }

    constexpr bool GetBlackKing() const
    {
        return _data[2];
    }

    constexpr bool GetBlackQueen() const
    {
        return _data[3];
    }

    constexpr bool CanCastleLike(Piece const piece) const
    {
        if (piece == WhiteKing)
            return GetWhiteKing();
        if (piece == WhiteQueen)
            return GetWhiteQueen();
        if (piece == BlackKing)
            return GetBlackKing();
        if (piece == BlackQueen)
            return GetBlackQueen();

        Unreachable();
    }

    constexpr bool CanCastle(Color const color) const
    {
        if (color == White)
            return GetWhiteKing() || GetWhiteQueen();
        
        if (color == Black)
            return GetBlackKing() || GetBlackQueen();
        
        Unreachable();
    }

    void SetWhiteKing(bool value)
    {
        _data[0] = value;
    }

    void SetWhiteQueen(bool value)
    {
        _data[1] = value;
    }

    void SetBlackKing(bool value)
    {
        _data[2] = value;
    }

    void SetBlackQueen(bool value)
    {
        _data[3] = value;
    }

    void Set(Piece const piece, bool value)
    {
        if (piece == WhiteKing)
            SetWhiteKing(value);
        else if (piece == WhiteQueen)
            SetWhiteQueen(value);
        else if (piece == BlackKing)
            SetBlackKing(value);
        else if (piece == BlackQueen)
            SetBlackQueen(value);
        else
            Unreachable();
    }

    void UpdateFromBoard(Board const & board);
};
