#pragma once

#include <vector>
#include <algorithm>

#include "../BaseTypes.hpp"
#include "Constants.hpp"

class PvTable
{
    int _maxDepth;
    std::vector<SimpleMove> _table;
    std::vector<SimpleMove> _bestLine;

    void Resize(int newMaxDepth)
    {
        int oldMaxDepth = _maxDepth;
        // if we tried this algorithm with too small a newMaxDepth, we'd overwrite old values
        if (newMaxDepth <= oldMaxDepth)
            return;

        _table.resize(TableSize(newMaxDepth), SimpleMoveNull);

        // We order by depth decreasing, variant decreasing because then we are sure that the new index is always greater than the old index.
        // Depth == 0 does not need to be moved
        for (int variant = oldMaxDepth; variant > 0; variant--)
        {
            for (int depth = oldMaxDepth; depth >= variant; depth--)
            {
                _table[GetIndex(newMaxDepth, depth, variant)] = _table[GetIndex(oldMaxDepth, depth, variant)];
                _table[GetIndex(oldMaxDepth, depth, variant)] = SimpleMoveNull;
            }
        }

        _bestLine.resize(BestLineSize(newMaxDepth), SimpleMoveNull);

        _maxDepth = newMaxDepth;
    }

    int GetIndex(int maxDepth, int depth, int variant) const
    {
        assert(depth <= maxDepth);
        assert(variant <= depth);

        return maxDepth*variant + depth;
    }

    static constexpr size_t TableSize(int maxDepth)
    {
        return (maxDepth+1)*(maxDepth+1);
    }

    static constexpr size_t BestLineSize(int maxDepth)
    {
        return maxDepth+1;
    }

public:
    PvTable() : _maxDepth{Constants::PvTableInitialMaxDepth}, _table{TableSize(_maxDepth), SimpleMoveNull}, _bestLine{BestLineSize(_maxDepth), SimpleMoveNull} {}

    void SetBestLine(SimpleMove const & move)
    {
        _table[GetIndex(_maxDepth, 0, 0)] = move;
        int depth;
        for (depth = 0; depth <= _maxDepth; depth++)
        {
            _bestLine[depth] = _table[GetIndex(_maxDepth, depth, 0)];

            if (_table[GetIndex(_maxDepth, depth, 0)] == SimpleMoveNull)
                break;
        }

        for (; depth <= _maxDepth; depth++)
        {
            _bestLine[depth] = SimpleMoveNull;
        }
    }
    
    void ClearPv(int depth)
    {
        if (depth > _maxDepth)
        {
            Resize(static_cast<int>(_maxDepth * 1.5)); // roughly double the storage space used
        }

        _table[GetIndex(_maxDepth, depth, depth)] = SimpleMoveNull;
    }

    void SetPv(int const currentDepth, SimpleMove const & move)
    {
        if (currentDepth+1 > _maxDepth)
        {
            Resize(static_cast<int>(_maxDepth * 1.5)); // roughly double the storage space used
        }

        _table[GetIndex(_maxDepth, currentDepth, currentDepth)] = move;

        for (int depth = currentDepth+1; depth <= _maxDepth; depth++)
        {
            _table[GetIndex(_maxDepth, depth, currentDepth)] = _table[GetIndex(_maxDepth, depth, currentDepth+1)];
            
            if (_table[GetIndex(_maxDepth, depth, currentDepth+1)] == SimpleMoveNull) 
                break;
        }
    }

    std::vector<SimpleMove> GetBestLine() const
    {
        std::vector<SimpleMove> ret;
        for (auto const & move : _bestLine)
        {
            if (move == SimpleMoveNull)
                break;

            ret.push_back(move);
        }
        return ret;
    }

    void Clear()
    {
        for (SimpleMove & move : _bestLine)
        {
            move = SimpleMoveNull;
        }

        for (SimpleMove & move : _table)
        {
            move = SimpleMoveNull;
        }
    }

    SimpleMove GetBestMove(int depth = 0) const
    {
        if (depth <= _maxDepth)
            return _bestLine[depth];
        
        return SimpleMoveNull;
    }


};
