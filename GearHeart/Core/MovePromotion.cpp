
#include "../Core.hpp"

bool MovePromotion::IsCapture() const
{
    return _capturedPiece != Space;
}

Piece MovePromotion::GetCapturedPiece() const
{
    return _capturedPiece;
}

bool MovePromotion::IsPawnMove() const
{
    return _piece.GetType() == Pawn;
}

PieceType MovePromotion::PromotionPieceType() const
{
    return _promotionPieceType;
}

template <IsSearching isSearching>
void MovePromotion::PerformUpdateBoardPieces(Board & board) const
{
    if (IsCapture())
        board.RemovePiece<isSearching>(_dstCoord);

    board.RemovePiece<isSearching>(_srcCoord);
    board.AddPiece<isSearching>(_dstCoord, _piece.GetColor() + _promotionPieceType);
}

template <IsSearching isSearching>
void MovePromotion::UndoUpdateBoardPieces(Board & board) const
{
    board.RemovePiece<isSearching>(_dstCoord);
    board.AddPiece<isSearching>(_srcCoord, _piece);

    if (IsCapture())
        board.AddPiece<isSearching>(_dstCoord, _capturedPiece);
}

void MovePromotion::PerformUpdateBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::Searching>(board);
}

void MovePromotion::UndoUpdateBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::Searching>(board);
}

void MovePromotion::PerformTestBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::TestingMove>(board);
}

void MovePromotion::UndoTestBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::TestingMove>(board);
}