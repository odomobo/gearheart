#pragma once

#include <chrono>
#include <optional>

#include "../BaseTypes.hpp"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::high_resolution_clock::time_point TimePoint;
typedef std::chrono::high_resolution_clock::duration Duration;

struct Timing
{
    TimePoint SearchStartTime;
    // this is optional because it doesn't get set when pondering until a ponder hit
    std::optional<TimePoint> ClockStartTime{};
    std::optional<Duration> SearchDuration{};
    // If this is set to false, then we can stop searching early if prudent. Not fully implemented.
    bool IsSearchTimeExact{}; 
    std::optional<int> MaxSearchDepth{};
    std::optional<uint64_t> MaxSearchNodes{};
    std::optional<int> SearchForMateIn{};
    bool Infinite{};
};
