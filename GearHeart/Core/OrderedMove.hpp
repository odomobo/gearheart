#pragma once

#include "../BaseTypes.hpp"
#include "MoveContainer.hpp"

class Order
{
    uint32_t _value;
public:
    constexpr Order(uint8_t primary, uint8_t secondary, uint8_t tertiary, uint8_t quaternary) : _value{static_cast<uint32_t>(primary << 24) | static_cast<uint32_t>(secondary << 16) | static_cast<uint32_t>(tertiary << 8) | quaternary} {}
    constexpr Order(uint8_t primary, uint8_t secondary, uint8_t tertiary) : Order{primary, secondary, tertiary, 0} {}
    constexpr Order(uint8_t primary, uint8_t secondary) : Order{primary, secondary, 0} {}
    constexpr Order(uint8_t primary) : Order{primary, 0} {}

    constexpr bool operator<(Order const other) const
    {
        return _value < other._value;
    }
};

class OrderedMove
{
    MoveContainer _move;
public:
    Order _Order;
    
    template <typename ... TArgs>
    OrderedMove(TArgs ... args) : _move{args...}, _Order{0} {}

    constexpr bool operator<(OrderedMove const & other) const
    {
        return _Order < other._Order;
    }

    MoveBase * operator->()
    {
        return _move.operator->();
    }

    MoveBase const * operator->() const
    {
        return _move.operator->();
    }

    MoveBase & operator*()
    {
        return _move.operator*();
    }

    MoveBase const & operator*() const
    {
        return _move.operator*();
    }
};