#pragma once

#include "../BaseTypes.hpp"

#include "MoveBase.hpp"
#include "MoveNormal.hpp"
#include "MoveCastling.hpp"
#include "MoveEnPassant.hpp"
#include "MovePromotion.hpp"

typedef PolyContainer<
    MoveBase, 
    MoveNormal, 
    MoveCastling, 
    MoveEnPassant,
    MovePromotion
> MoveContainer;
