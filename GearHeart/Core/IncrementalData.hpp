#pragma once

#include "../BaseTypes.hpp"
#include "ZobristHash.hpp"

struct IncrementalData
{
    ZobristHash _ZobristPieces{};
    WhiteScore _PartialScore{};
};
