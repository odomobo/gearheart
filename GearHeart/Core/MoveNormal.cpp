
#include "../Core.hpp"

bool MoveNormal::IsDoublePawnMove() const
{
    int ranks = (_dstCoord - _srcCoord).GetRank();
    return IsPawnMove() && std::abs(ranks) == 2;
}

bool MoveNormal::IsCapture() const
{
    return _capturedPiece != Space;
}

Piece MoveNormal::GetCapturedPiece() const
{
    return _capturedPiece;
}

bool MoveNormal::IsPawnMove() const
{
    return _piece.GetType() == Pawn;
}

PieceType MoveNormal::PromotionPieceType() const
{
    return _promotionPieceType;
}

Coord MoveNormal::DoublePawnMoveEnPassantCoord() const
{
    if (IsDoublePawnMove())
    {
        Offset moveOffset = _dstCoord - _srcCoord;
        return _srcCoord + (moveOffset/2);
    }
    else
    {
        return CoordInvalid;
    }
}

template <IsSearching isSearching>
void MoveNormal::PerformUpdateBoardPieces(Board & board) const
{
    if (IsCapture())
        board.RemovePiece<isSearching>(_dstCoord);

    board.MovePiece<isSearching>(_srcCoord, _dstCoord);
}

template <IsSearching isSearching>
void MoveNormal::UndoUpdateBoardPieces(Board & board) const
{
    board.MovePiece<isSearching>(_dstCoord, _srcCoord);

    if (IsCapture())
        board.AddPiece<isSearching>(_dstCoord, _capturedPiece);
}

void MoveNormal::PerformUpdateBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveNormal::UndoUpdateBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveNormal::PerformTestBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::TestingMove>(board);
}

void MoveNormal::UndoTestBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::TestingMove>(board);
}