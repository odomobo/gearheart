#pragma once

#include "../BaseTypes.hpp"
#include "MoveBase.hpp"

class Board;

class MoveCastling : public MoveBase
{
    Coord const _rookSrcCoord;

    template <IsSearching isSearching>
    void PerformUpdateBoardPieces(Board & board) const;

    template <IsSearching isSearching>
    void UndoUpdateBoardPieces(Board & board) const;

public:
    constexpr MoveCastling(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord,
        Coord const rookSrcCoord
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _rookSrcCoord{rookSrcCoord}
    {}

    bool IsCapture() const override;
    bool IsPawnMove() const override;

    void PerformUpdateBoardPieces(Board & board) const override;
    void UndoUpdateBoardPieces(Board & board) const override;
    void PerformTestBoardPieces(Board & board) const override;
    void UndoTestBoardPieces(Board & board) const override;
};