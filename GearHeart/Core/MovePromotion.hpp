#pragma once

#include "../BaseTypes.hpp"
#include "MoveBase.hpp"

class Board;

class MovePromotion : public MoveBase
{
    Piece const _capturedPiece;
    PieceType const _promotionPieceType;

    template <IsSearching isSearching>
    void PerformUpdateBoardPieces(Board & board) const;

    template <IsSearching isSearching>
    void UndoUpdateBoardPieces(Board & board) const;

public:
    constexpr MovePromotion(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord,
        PieceType const promotionPieceType
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _promotionPieceType{promotionPieceType},
        _capturedPiece{Space}
    {}

    constexpr MovePromotion(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord,
        PieceType const promotionPieceType,
        Piece const capturedPiece
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _promotionPieceType{promotionPieceType},
        _capturedPiece{capturedPiece}
    {}

    bool IsCapture() const override;
    Piece GetCapturedPiece() const override;
    bool IsPawnMove() const override;
    PieceType PromotionPieceType() const override;

    void PerformUpdateBoardPieces(Board & board) const override;
    void UndoUpdateBoardPieces(Board & board) const override;
    void PerformTestBoardPieces(Board & board) const override;
    void UndoTestBoardPieces(Board & board) const override;
};