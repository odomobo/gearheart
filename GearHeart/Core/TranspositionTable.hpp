#pragma once

#include <optional>

#include "../BaseTypes.hpp"
#include "ZobristHash.hpp"
#include <winerror.h>

enum class TTEntryType : uint8_t
{
    AllNode,
    PvNode,
    CutNode
};

struct alignas(16) TTEntry
{
    ZobristHash _Hash{};
    TTEntryType _NodeType{};
    int16_t _CentiDepth{};
    TinyScore _Score{};
    TinyMove _Move{TinyMoveNull};
};
static_assert(sizeof(TTEntry) == 16, "TTEntry should be equal to 16 bytes. Remove this check if you want to add more data to it.");

class TranspositionTable
{
    size_t _size;
    TTEntry * _data;

    size_t GetIndex(ZobristHash const hash) const
    {
        return static_cast<uint64_t>(hash) % _size;
    }

    static size_t SizeInMbToSize(int sizeInMb)
    {
        int sizeInBytes = sizeInMb * (1 << 20);
        auto size = sizeInBytes / sizeof(*_data);

        // Ensure size is always at least 1, although this is kind of a silly transposition table.
        // It's better than crashing or corrupting on 0
        if (size == 0)
            size = 1;

        return size;
    }

public:
    TranspositionTable(int sizeInMb)
    {
        _size = SizeInMbToSize(sizeInMb);

        _data = new TTEntry[_size];
        Clear();
    }

    ~TranspositionTable()
    {
        delete[] _data;
    }

    void Resize(int sizeInMb)
    {
        auto newSize = SizeInMbToSize(sizeInMb);
        if (newSize == _size)
            return;

        delete[] _data;
        _size = newSize;
        _data = new TTEntry[_size];
        Clear();
    }

    void Clear()
    {
        for (size_t i = 0; i < _size; i++)
        {
            _data[i] = TTEntry{};
        }
    }

    std::optional<TTEntry> TryGetEntry(ZobristHash const hash) const
    {
        TTEntry entry = _data[GetIndex(hash)];
        if (entry._Hash != hash)
            return std::nullopt;

        return entry;
    }

    void MaybeSetEntry(ZobristHash hash, Score score, TTEntryType nodeType, SimpleMove bestMove, int centiDepth, int ply)
    {
        // Normally, mate scores are calculated as distance to root.
        // We need to reduce it to the distance from this position. Then, we increase it by the appropriate amount when getting it from the transposition table.
        Score ttScore = score.GetReducedDistanceToMateBy(ply);
        TTEntry ttEntry{hash, nodeType, static_cast<int16_t>(centiDepth), ttScore, bestMove};
        MaybeSetEntry(ttEntry);
    }

    // This doesn't set an entry if it doesn't work for the current replacement scheme.
    // However, our current replacement scheme is "always replace".
    void MaybeSetEntry(TTEntry & entry)
    {
        _data[GetIndex(entry._Hash)] = entry;
    }
};
