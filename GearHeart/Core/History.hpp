#pragma once

#include "../BaseTypes.hpp"
#include "CastlingRights.hpp"
#include "ZobristHash.hpp"

// TODO: name something better? Transients? Or what?
struct History
{
    CastlingRights _CastlingRights;
    Coord _EnPassant;
    Coord _LastDestinationSquare;
    // Used for the 50 move rule
    int _Halfmoves{};
    ZobristHash _ZobristHash;
};
