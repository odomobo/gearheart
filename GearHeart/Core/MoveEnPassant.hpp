#pragma once

#include "../BaseTypes.hpp"
#include "MoveBase.hpp"

class Board;

class MoveEnPassant : public MoveBase
{
    Coord const _capturedCoord;

    template <IsSearching isSearching>
    void PerformUpdateBoardPieces(Board & board) const;

    template <IsSearching isSearching>
    void UndoUpdateBoardPieces(Board & board) const;

public:
    constexpr MoveEnPassant(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _capturedCoord{dstCoord.GetFile(), srcCoord.GetRank()}
    {}

    bool IsCapture() const override;
    Piece GetCapturedPiece() const override;
    bool IsPawnMove() const override;
    
    void PerformUpdateBoardPieces(Board & board) const override;
    void UndoUpdateBoardPieces(Board & board) const override;
    void PerformTestBoardPieces(Board & board) const override;
    void UndoTestBoardPieces(Board & board) const override;
};
