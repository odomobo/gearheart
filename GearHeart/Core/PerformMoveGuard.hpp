#pragma once

#include "../BaseTypes.hpp"

#include "MoveBase.hpp"

class Board;

template <IsSearching TSearching>
class PerformMoveGuard
{
    Board & _board;
    MoveBase const & _move;

public:
    explicit PerformMoveGuard(Board & board, MoveBase const & move) : _board{board}, _move{move}
    {
        _move.PerformMove<TSearching>(_board);
    }

    // prevent shenannigans like copying and moving
    PerformMoveGuard() = delete;
    PerformMoveGuard(const PerformMoveGuard& other) = delete;
    PerformMoveGuard(PerformMoveGuard&& other) noexcept = delete;
    PerformMoveGuard& operator=(const PerformMoveGuard& other) = delete;
    PerformMoveGuard& operator=(PerformMoveGuard&& other) noexcept = delete;

    ~PerformMoveGuard()
    {
        _move.UndoMove<TSearching>(_board);
    }
};