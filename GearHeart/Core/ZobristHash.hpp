#pragma once

#include "../BaseTypes.hpp"
#include "CastlingRights.hpp"

class Board;

class ZobristHash
{
    uint64_t _value;

    static std::array<uint64_t, 781> const Keys;

    // this looks like a funky algorithm, and it is. The zobrist keys are:
    // pPnNbBrRqQkK
    // but PieceType uses the format:
    // _pnbrqk
    static constexpr size_t ZobristHashPieceIndex(Piece const piece)
    {
        size_t spacedIndexes = (piece.GetType().GetIndex()-1) * 2;
        if (piece.GetColor() == Black)
            return spacedIndexes;
        if (piece.GetColor() == White)
            return spacedIndexes+1;

        Unreachable();
    }

public:
    ZobristHash() : _value{0} {}
    constexpr explicit ZobristHash(uint64_t value) : _value{value} {}
    constexpr explicit operator uint64_t() const
    {
        return _value;
    }

    static ZobristHash FromBoard(Board const & board);
    static ZobristHash FromPieces(Board const & board);
    static ZobristHash FromPiece(Piece const piece, Coord const coord);
    static ZobristHash FromCastling(CastlingRights const & castlingRights);
    static ZobristHash FromCastlingDirection(Piece const piece);
    static ZobristHash FromEnPassant(Coord const coord);
    static ZobristHash FromToMove(Color const color);
    static ZobristHash ToMove();

    constexpr bool operator==(ZobristHash const other) const
    {
        return _value == other._value;
    }

    constexpr bool operator!=(ZobristHash const other) const
    {
        return _value != other._value;
    }

    constexpr ZobristHash operator^(ZobristHash const other) const
    {
        return ZobristHash{_value ^ other._value};
    }

    ZobristHash & operator^=(ZobristHash const other)
    {
        _value ^= other._value;
        return *this;
    }
};

// this allows ZobristHash to be hashable
namespace std
{

template <>
struct hash<ZobristHash>
{
    size_t operator()(ZobristHash const zobristHash) const
    {
        uint64_t hashUint64 = static_cast<uint64_t>(zobristHash);
        return static_cast<size_t>(hashUint64);
    }
};

}