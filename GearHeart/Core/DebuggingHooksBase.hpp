#pragma once

#include <string>

class Board;

class DebuggingHooksBase
{
public:
    virtual ~DebuggingHooksBase() = default;
    virtual void BoardReset(Board const & board) {};
    virtual void AfterBoardPieceModified(Board const & board) {};
    virtual void AfterMoveApplied(Board const & board) {};
};

// TODO: put this in Engine instead of having global
extern DebuggingHooksBase & DebuggingHooks;