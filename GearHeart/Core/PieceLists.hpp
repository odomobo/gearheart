#pragma once

#include <boost/container/static_vector.hpp>
#include <boost/container/small_vector.hpp>

#include "../BaseTypes.hpp"

// safest (won't crash, no matter how many pieces of a given type are on the board), but there's 1 extra pointer dereference each time you access the vector
//typedef NonCopyableContainer<Coord, boost::container::small_vector<Coord, 8> > piece_list_t;

// pretty safe (won't crash until 11 pieces of a given type are on the board, which isn't possible in a normal game)
typedef NonCopyableContainer<Coord, boost::container::static_vector<Coord, 10> > piece_list_t;

// most performant (it'll crash, for example, if you let the engine promote 8 queens)
//typedef NonCopyableContainer<Coord, boost::container::static_vector<Coord, 8> > piece_list_t;


// we technically only need the array to be of length 7, but if we use length 8 then the compiler can optimize index lookups
typedef NonCopyableContainer<piece_list_t, std::array<piece_list_t, 8> > piece_lists_by_color_t;

class PieceLists
{
    std::array<piece_lists_by_color_t, 2> _data;

public:

    piece_list_t & GetPieceList(Color const color, PieceType const type)
    {
        return _data[color.GetIndex()][type.GetIndex()];
    }

    piece_list_t const & GetPieceList(Color const color, PieceType const type) const
    {
        auto colorIndex = color.GetIndex();
        auto typeIndex = static_cast<_piece_t>(type);
        return _data[colorIndex][typeIndex];
    }

    piece_list_t & GetPieceList(Piece const piece)
    {
        return GetPieceList(piece.GetColor(), piece.GetType());
    }

    piece_list_t const & GetPieceList(Piece const piece) const
    {
        return GetPieceList(piece.GetColor(), piece.GetType());
    }

    piece_lists_by_color_t & GetPieceListsByColor(Color const color)
    {
        auto colorIndex = color.GetIndex();
        return _data[colorIndex];
    }

    piece_lists_by_color_t const & GetPieceListsByColor(Color const color) const
    {
        auto colorIndex = color.GetIndex();
        return _data[colorIndex];
    }

    void Add(Piece const piece, Coord const coord)
    {
        auto & pieceList = GetPieceList(piece);
        pieceList.push_back(coord);
    }

    void Update(Piece const piece, Coord const srcCoord, Coord const dstCoord)
    {
        auto & pieceList = GetPieceList(piece);
        for (int i = static_cast<int>(std::size(pieceList)) - 1; i >= 0; i--)
        {
            if (pieceList[i] == srcCoord)
            {
                pieceList[i] = dstCoord;
                return;
            }
        }

        Unreachable();
    }

    void Remove(Piece const piece, Coord const coord)
    {
        auto & pieceList = GetPieceList(piece);
        for (int i = static_cast<int>(std::size(pieceList)) - 1; i >= 0; i--)
        {
            if (pieceList[i] == coord)
            {
                pieceList[i] = pieceList.back();
                pieceList.pop_back();
                return;
            }
        }

        Unreachable();
    }

    std::optional<size_t> GetIndex(Piece const piece, Coord const coord) const
    {
        auto const & pieceList = GetPieceList(piece);
        for (size_t index = 0; index < std::size(pieceList); index++)
        {
            if (pieceList[index] == coord)
                return index;
        }
        return std::nullopt;
    }

    void Clear()
    {
        for (auto & pieceListsByColor : _data)
        {
            for (auto & pieceList : pieceListsByColor)
            {
                pieceList.clear();
            }
        }
    }

    static constexpr size_t const FirstIndex{1};
    static constexpr size_t const LastIndex{6};
};