#pragma once

#include <string>

#include "../BaseTypes.hpp"

struct Info;

class ProtocolInterface
{
protected:
    virtual void SendInfoStringImpl(std::string const & message) const = 0;
public:
    virtual ~ProtocolInterface() = default;
    virtual void SendInfo(Score const score) const = 0;
    virtual void SendBestMove(SimpleMove const & move, SimpleMove const & ponderMove = SimpleMoveNull) const = 0;
    
    template <typename ... TArgs>
    void SendInfoString(TArgs ... args) const
    {
        SendInfoStringImpl(fmt::format(args...));
    };
};