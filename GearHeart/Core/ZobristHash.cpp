#include "../Core.hpp"

ZobristHash ZobristHash::FromBoard(Board const & board)
{
    ZobristHash ret = board._IncrementalData._ZobristPieces;
    ret ^= FromCastling(board.GetCastlingRights());
    ret ^= FromEnPassant(board.GetEnPassantCoord());
    ret ^= FromToMove(board.GetToMove());
    return ret;
}

ZobristHash ZobristHash::FromPieces(Board const& board)
{
    ZobristHash ret;

    for (Color color : Colors)
    {
        for (PieceType type : PieceTypes)
        {
            Piece piece = color+type;
            for (Coord coord : board.GetPieceList(piece))
            {
                ret ^= FromPiece(piece, coord);
            }
        }
    }
    return ret;
}

ZobristHash ZobristHash::FromPiece(Piece const piece, Coord const coord)
{
    auto zobristPieceIndex = ZobristHashPieceIndex(piece);
    auto index = 64*zobristPieceIndex + coord.Get64ArrayIndex();
    return ZobristHash{Keys[index]};
}

ZobristHash ZobristHash::FromCastling(CastlingRights const & castlingRights)
{
    ZobristHash ret;
    for (Piece castlingDirection : {WhiteKing, WhiteQueen, BlackKing, BlackQueen})
    {
        if (castlingRights.CanCastleLike(castlingDirection))
            ret ^= FromCastlingDirection(castlingDirection);
    }
    return ret;
}

ZobristHash ZobristHash::FromCastlingDirection(Piece const piece)
{
    size_t offset;
    if (piece == WhiteKing)
    {
        offset = 0;
    }
    else if (piece == WhiteQueen)
    {
        offset = 1;
    }
    else if (piece == BlackKing)
    {
        offset = 2;
    }
    else if (piece == BlackQueen)
    {
        offset = 3;
    }
    else
    {
        Unreachable();
    }
    return ZobristHash{Keys[768 + offset]};
}

ZobristHash ZobristHash::FromEnPassant(Coord const coord)
{
    if (coord.IsInvalid())
        return ZobristHash{};

    return ZobristHash{Keys[772 + coord.GetFile()]};
}

ZobristHash ZobristHash::FromToMove(Color const color)
{
    if (color == White){
        return ToMove();
    }
    if (color == Black)
    {
        return ZobristHash{};
    }

    Unreachable();
}


ZobristHash ZobristHash::ToMove()
{
    return ZobristHash{Keys[780]};
}
