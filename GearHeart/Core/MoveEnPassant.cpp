
#include "../Core.hpp"

bool MoveEnPassant::IsCapture() const
{
    return true;
}

Piece MoveEnPassant::GetCapturedPiece() const
{
    return ~_piece.GetColor() + Pawn;
}

bool MoveEnPassant::IsPawnMove() const
{
    return true;
}

template <IsSearching isSearching>
void MoveEnPassant::PerformUpdateBoardPieces(Board & board) const
{
    board.RemovePiece<isSearching>(_capturedCoord);
    board.MovePiece<isSearching>(_srcCoord, _dstCoord);
}

template <IsSearching isSearching>
void MoveEnPassant::UndoUpdateBoardPieces(Board & board) const
{
    board.MovePiece<isSearching>(_dstCoord, _srcCoord);

    // _piece is the piece that we used to perform the capture, not the piece which was captured. We need to add an opposite-color pawn back on _capturedCoord
    Color otherColor = ~_piece.GetColor();
    board.AddPiece<isSearching>(_capturedCoord, otherColor + Pawn);
}

void MoveEnPassant::PerformUpdateBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveEnPassant::UndoUpdateBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveEnPassant::PerformTestBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::TestingMove>(board);
}

void MoveEnPassant::UndoTestBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::TestingMove>(board);
}