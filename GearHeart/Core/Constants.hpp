#pragma once

#include "../BaseTypes.hpp"

#define ENABLED_DEBUGGING_PLUGINS
//#define ENABLED_DEBUGGING_PLUGINS Example
//#define ENABLED_DEBUGGING_PLUGINS PieceListConsistency
//#define ENABLED_DEBUGGING_PLUGINS IncrementalConsistency

namespace Constants
{

constexpr int const SearchCheckPeriod = 4096;
constexpr int const PvTableInitialMaxDepth = 10;

std::string const EngineName = "GearHeart";
std::string const Version = "0.16";
std::string const Author = "Josh Odom";

}