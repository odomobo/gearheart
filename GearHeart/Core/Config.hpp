#pragma once

#include <string>

#include "../BaseTypes.hpp"

// _Config's fields need to stay in sync with UciConfig's options
struct Config
{
    int _TimeBufferMillis;
    bool _Ponder;
    int _HashMb;
    int _Contempt;
};
