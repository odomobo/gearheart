
#include "../Core.hpp"

bool MoveCastling::IsCapture() const
{
    return false;
}

bool MoveCastling::IsPawnMove() const
{
    return false;
}

template <IsSearching isSearching>
void MoveCastling::PerformUpdateBoardPieces(Board & board) const
{
    Coord rookDstCoord = _srcCoord + ((_dstCoord - _srcCoord) / 2);

    board.MovePiece<isSearching>(_srcCoord, _dstCoord);
    board.MovePiece<isSearching>(_rookSrcCoord, rookDstCoord);
}

template <IsSearching isSearching>
void MoveCastling::UndoUpdateBoardPieces(Board & board) const
{
    Piece king = _piece;
    Piece rook = Rook + _piece.GetColor();

    Coord rookDstCoord = _srcCoord + ((_dstCoord - _srcCoord) / 2);

    board.MovePiece<isSearching>(rookDstCoord, _rookSrcCoord);
    board.MovePiece<isSearching>(_dstCoord, _srcCoord);
}

void MoveCastling::PerformUpdateBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveCastling::UndoUpdateBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::Searching>(board);
}

void MoveCastling::PerformTestBoardPieces(Board & board) const
{
    PerformUpdateBoardPieces<IsSearching::TestingMove>(board);
}

void MoveCastling::UndoTestBoardPieces(Board & board) const
{
    UndoUpdateBoardPieces<IsSearching::TestingMove>(board);
}