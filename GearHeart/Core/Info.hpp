#pragma once

#include "../BaseTypes.hpp"

struct Info
{
    uint64_t NodesVisited;
    int Depth;
    int SelDepth;
    Score LastRootPvScore;
};