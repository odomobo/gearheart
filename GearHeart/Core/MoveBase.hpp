#pragma once

#include "../BaseTypes.hpp"

#include "Board.hpp"

class MoveBase
{
protected:
    Piece const _piece;
    Coord const _srcCoord;
    Coord const _dstCoord;

    virtual void PerformUpdateBoardPieces(Board & board) const = 0;
    virtual void UndoUpdateBoardPieces(Board & board) const = 0;
    virtual void PerformTestBoardPieces(Board & board) const = 0;
    virtual void UndoTestBoardPieces(Board & board) const = 0;

public:
    constexpr MoveBase(Piece const piece, Coord const srcCoord, Coord const dstCoord) :
        _piece{piece},
        _srcCoord{srcCoord},
        _dstCoord{dstCoord}
    {}

    operator SimpleMove() const
    {
        return SimpleMove{_srcCoord, _dstCoord, PromotionPieceType()};
    }

    operator TinyMove() const
    {
        return TinyMove{_srcCoord, _dstCoord};
    }

    // This is non-virtual, because in order to be efficient, all children should be POD classes
    ~MoveBase() = default;
    virtual bool IsCapture() const = 0;
    virtual Piece GetCapturedPiece() const
    {
        return Space;
    }
    virtual bool IsPawnMove() const = 0;
    virtual PieceType PromotionPieceType() const
    {
        return PieceTypeNone;
    }
    virtual Coord DoublePawnMoveEnPassantCoord() const
    {
        return CoordInvalid;
    }
    
    template <IsSearching TSearching>
    void PerformMove(Board & board) const
    {
        if (TSearching == IsSearching::TestingMove)
        {
            PerformTestBoardPieces(board);
        }
        else
        {
            PerformUpdateBoardPieces(board);
            board.SwapToMove();
            // check color after swapping
            if (board.GetToMove() == White)
            {
                board.IncrementMoveNumber();
            }

            board.PushHistory();

            if (IsCapture() || IsPawnMove())
                board.ResetHalfmoves();
            else
                board.IncrementHalfmoves();

            board.SetEnPassantCoord(DoublePawnMoveEnPassantCoord());
            board.UpdateCastlingRights();
            board.SetLastDestinationSquare(_dstCoord);
            board.UpdateZobristHash();
            board.AddRepetition<TSearching>();

            DebuggingHooks.AfterMoveApplied(board);
        }
    }

    template <IsSearching TSearching>
    void UndoMove(Board & board) const
    {
        if (TSearching == IsSearching::TestingMove)
        {
            UndoTestBoardPieces(board);
        }
        else
        {
            board.RemoveRepetition<TSearching>();
            board.PopHistory();

            // check color before swapping
            if (board.GetToMove() == White)
            {
                board.DecrementMoveNumber();
            }
            board.SwapToMove();
            UndoUpdateBoardPieces(board);

            DebuggingHooks.AfterMoveApplied(board);
        }
    }

    Piece GetPiece() const
    {
        return _piece;
    }
    Coord SrcCoord() const
    {
        return _srcCoord;
    }
    Coord DstCoord() const
    {
        return _dstCoord;
    }
};

inline bool operator==(MoveBase const & lhs, SimpleMove const & rhs)
{
    SimpleMove const simpleLhs = lhs;
    return simpleLhs == rhs;
}

inline bool operator!=(MoveBase const & lhs, SimpleMove const & rhs)
{
    return !(lhs == rhs);
}

inline bool operator==(SimpleMove const & lhs, MoveBase const & rhs)
{
    return rhs == lhs;
}

inline bool operator!=(SimpleMove const & lhs, MoveBase const & rhs)
{
    return rhs != lhs;
}