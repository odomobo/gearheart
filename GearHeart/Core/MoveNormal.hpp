#pragma once

#include "../BaseTypes.hpp"
#include "MoveBase.hpp"

class Board;

class MoveNormal : public MoveBase
{
    Piece const _capturedPiece;
    PieceType const _promotionPieceType;

    bool IsDoublePawnMove() const;

    template <IsSearching isSearching>
    void PerformUpdateBoardPieces(Board & board) const;

    template <IsSearching isSearching>
    void UndoUpdateBoardPieces(Board & board) const;

public:

    constexpr MoveNormal(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _capturedPiece{Space},
        _promotionPieceType{PieceTypeNone}
    {}

    constexpr MoveNormal(
        Piece const piece,
        Coord const srcCoord,
        Coord const dstCoord,
        Piece const capturePiece
    ) :
        MoveBase{piece, srcCoord, dstCoord},
        _capturedPiece{capturePiece},
        _promotionPieceType{PieceTypeNone}
    {}

    bool IsCapture() const override;
    Piece GetCapturedPiece() const override;
    bool IsPawnMove() const override;
    PieceType PromotionPieceType() const override;
    Coord DoublePawnMoveEnPassantCoord() const override;

    void PerformUpdateBoardPieces(Board & board) const override;
    void UndoUpdateBoardPieces(Board & board) const override;
    void PerformTestBoardPieces(Board & board) const override;
    void UndoTestBoardPieces(Board & board) const override;
};