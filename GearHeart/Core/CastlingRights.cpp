#include "../Core.hpp"

void CastlingRights::UpdateFromBoard(Board const & board)
{
    for (Color color : Colors)
    {
        if (!CanCastle(color))
            continue;

        Coord kingCoord = color == White ? e1 : e8;
        Coord kingsideRookCoord = color == White ? h1 : h8;
        Coord queensideRookCoord = color == White ? a1 : a8;

        if (board[kingCoord] != color+King)
        {
            Set(color+King, false);
            Set(color+Queen, false);
        }

        if (board[kingsideRookCoord] != color+Rook)
        {
            Set(color+King, false);
        }

        if (board[queensideRookCoord] != color+Rook)
        {
            Set(color+Queen, false);
        }
    }
}