#pragma once

#include <array>
#include <vector>

#include "../BaseTypes.hpp"
#include "IncrementalData.hpp"
#include "History.hpp"
#include "PieceLists.hpp"
#include "Repetitions.hpp"

// this is needed for incremental score updates
#include "../Engine/Evaluate.hpp"

class Board
{
    
    std::array<Piece, 128> _board;
    Color _toMove;
    Repetitions _repetitions;
    bool _isRepetiton;
    std::vector<History> _historyStack;
    int _moveNumber;
    PieceLists _pieceLists;
    
public:

    IncrementalData _IncrementalData;

    Board() : 
        _board{},
        _toMove{White},
        _repetitions{},
        _isRepetiton{false},
        _historyStack{},
        _moveNumber{1},
        _pieceLists{}
    {
        _historyStack.reserve(1024); // this probably won't need to grow if it starts having 1024 slots
        _historyStack.emplace_back();
        
        DebuggingHooks.BoardReset(*this);
    }

    ~Board() = default;

    // delete copy and move operators, because we never want to do this
    Board(const Board& other) = delete;
    Board(Board&& other) noexcept = delete;
    Board& operator=(const Board& other) = delete;
    Board& operator=(Board&& other) noexcept = delete;

    void Clear()
    {
        _board.fill(Space);
        _toMove = White;
        _repetitions.Clear();
        _isRepetiton = false;
        _historyStack.clear();
        _moveNumber = 1;
        _pieceLists.Clear();
        _IncrementalData = {};

        _historyStack.emplace_back();

        DebuggingHooks.BoardReset(*this);
    }

    template <IsSearching isSearching = IsSearching::Moving>
    void AddPiece(Coord const coord, Piece const piece)
    {
        _board[coord.GetArrayIndex()] = piece;

        _pieceLists.Add(piece, coord);

        if constexpr (isSearching != IsSearching::TestingMove)
        {
            _IncrementalData._ZobristPieces ^= ZobristHash::FromPiece(piece, coord);
            _IncrementalData._PartialScore += AddPiecePartialScoreDiff(piece, coord);
        }

        DebuggingHooks.AfterBoardPieceModified(*this);
    }

    template <IsSearching isSearching = IsSearching::Moving>
    void MovePiece(Coord const src, Coord const dst)
    {
        Piece piece = _board[src.GetArrayIndex()];

        _board[src.GetArrayIndex()] = Space;
        _board[dst.GetArrayIndex()] = piece;

        auto & pieceList = _pieceLists.GetPieceList(piece);

        _pieceLists.Update(piece, src, dst);

        if constexpr (isSearching != IsSearching::TestingMove)
        {
            _IncrementalData._ZobristPieces ^= ZobristHash::FromPiece(piece, src);
            _IncrementalData._ZobristPieces ^= ZobristHash::FromPiece(piece, dst);
            _IncrementalData._PartialScore += MovePiecePartialScoreDiff(piece, src, dst);
        }

        DebuggingHooks.AfterBoardPieceModified(*this);
    }

    template <IsSearching isSearching = IsSearching::Moving>
    void RemovePiece(Coord const coord)
    {
        Piece removedPiece = _board[coord.GetArrayIndex()];

        _board[coord.GetArrayIndex()] = Space;

        _pieceLists.Remove(removedPiece, coord);

        if constexpr (isSearching != IsSearching::TestingMove)
        {
            _IncrementalData._ZobristPieces ^= ZobristHash::FromPiece(removedPiece, coord);
            _IncrementalData._PartialScore += RemovePiecePartialScoreDiff(removedPiece, coord);
        }

        DebuggingHooks.AfterBoardPieceModified(*this);
    }

    Piece GetPiece(Coord const coord) const
    {
        return _board[coord.GetArrayIndex()];
    }

    Piece GetPiece(int const file, int const rank) const
    {
        return GetPiece(Coord{file, rank});
    }

    piece_list_t & GetPieceList(Piece const piece)
    {
        return _pieceLists.GetPieceList(piece);
    }

    piece_list_t const & GetPieceList(Piece const piece) const
    {
        return _pieceLists.GetPieceList(piece);
    }

    size_t CountAllPieces() const
    {
        size_t sum = 0;
        for (Color color : Colors)
        {
            for (PieceType type : PieceTypes)
            {
                sum += std::size(_pieceLists.GetPieceList(color + type));
            }
        }
        return sum;
    }

    size_t CountPieces(Piece piece) const
    {
        return std::size(_pieceLists.GetPieceList(piece));
    }

    size_t CountPiecesByColor(Color color) const
    {
        size_t sum = 0;
        for (PieceType type : PieceTypes)
        {
            sum += std::size(_pieceLists.GetPieceList(color + type));
        }
        return sum;
    }

    Piece operator[] (Coord const coord) const
    {
        return GetPiece(coord);
    }

    void SetToMove(Color const color)
    {
        _toMove = color;
    }

    Color GetToMove() const
    {
        return _toMove;
    }

    void SwapToMove()
    {
        _toMove = ~_toMove;
    }

    bool CanCastle(Color const color) const
    {
        return _historyStack.back()._CastlingRights.CanCastle(color);
    }

    bool CanCastleLike(Piece const piece) const
    {
        return _historyStack.back()._CastlingRights.CanCastleLike(piece);
    }

    CastlingRights const & GetCastlingRights() const
    {
        return _historyStack.back()._CastlingRights;
    }

    void SetCastlingRights(CastlingRights const & castlingRights)
    {
        _historyStack.back()._CastlingRights = castlingRights;
    }

    bool CanWin(Color const color) const
    {
        if (
            CountPieces(Pawn + color) > 0
            || CountPieces(Bishop + color) >= 2 // technically not true, if both bishops are on the same color...
            || CountPieces(Rook + color) > 0
            || CountPieces(Queen + color) > 0
            )
        {
            return true;
        }

        if (CountPieces(Bishop + color) > 0 && CountPieces(Knight + color) > 0)
            return true;

        return false;
    }

    Coord GetEnPassantCoord() const
    {
        return _historyStack.back()._EnPassant;
    }

    void SetEnPassantCoord(Coord const coord)
    {
        _historyStack.back()._EnPassant = coord;
    }

    void SetLastDestinationSquare(Coord const coord)
    {
        _historyStack.back()._LastDestinationSquare = coord;
    }

    Coord GetLastDestinationSquare() const
    {
        return _historyStack.back()._LastDestinationSquare;
    }

    void PushHistory()
    {
        _historyStack.push_back(_historyStack.back());
    }

    void PopHistory()
    {
        _historyStack.pop_back();
    }

    int GetMoveNumber() const
    {
        return _moveNumber;
    }

    void SetMoveNumber(int const moveNumber)
    {
        _moveNumber = moveNumber;
    }

    void IncrementMoveNumber()
    {
        _moveNumber++;
    }

    void DecrementMoveNumber()
    {
        _moveNumber--;
    }

    void IncrementHalfmoves()
    {
        _historyStack.back()._Halfmoves++;
    }

    void ResetHalfmoves()
    {
        _historyStack.back()._Halfmoves = 0;
    }

    int GetHalfmoves() const
    {
        return _historyStack.back()._Halfmoves;
    }

    void SetHalfmoves(int halfmoves)
    {
        _historyStack.back()._Halfmoves = halfmoves;
    }

    void UpdateCastlingRights()
    {
        _historyStack.back()._CastlingRights.UpdateFromBoard(*this);
    }

    void UpdateZobristHash()
    {
        _historyStack.back()._ZobristHash = ZobristHash::FromBoard(*this);
    }

    ZobristHash GetZobristHash() const
    {
        return _historyStack.back()._ZobristHash;
    }

    template <IsSearching TSearching>
    void AddRepetition()
    {
        _isRepetiton = _repetitions.AddRepetition<TSearching>(_historyStack.back()._ZobristHash);
    }

    template <IsSearching TSearching>
    void RemoveRepetition()
    {
        _repetitions.RemoveRepetition<TSearching>(_historyStack.back()._ZobristHash);
        _isRepetiton = false;
    }

    bool IsRepetition() const
    {
        return _isRepetiton;
    }

    Coord GetKingCoord(Color const color) const
    {
        return _pieceLists.GetPieceList(color, King)[0];
    }
    
    bool HasBothKings() const
    {
        return CountPieces(WhiteKing) == 1 &&
            CountPieces(BlackKing) == 1;
    }

    // for debugging
    friend class PieceListConsistency;
};
