#pragma once

#include <sparsehash/dense_hash_map>

#include "../BaseTypes.hpp"
#include "ZobristHash.hpp"

struct RepetitionEntry
{
    int TotalCount;
    int SearchCount;
};

class Repetitions
{
    google::dense_hash_map<ZobristHash, RepetitionEntry> _table;

public:
    Repetitions() : _table{}
    {
        // These are both a requirement for dense_hash_map.
        // Basically, these keys can never be used for real values.
        _table.set_empty_key(ZobristHash{0});
        _table.set_deleted_key(ZobristHash{0xffffffffffffffffull});
    }

    // Returns true on draw by repetition, false otherwise
    template <IsSearching TSearching>
    bool AddRepetition(ZobristHash const zobristHash)
    {
        RepetitionEntry & entry = _table[zobristHash];
        entry.TotalCount++;
        if (TSearching == IsSearching::Searching)
        {
            entry.SearchCount++;
            if (entry.SearchCount > 1)
            {
                return true;
            }
        }
        return entry.TotalCount >= 3;
    }

    template <IsSearching TSearching>
    void RemoveRepetition(ZobristHash const zobristHash)
    {
        // If we're going to delete an entry, we don't want a reference to it when we delete it
        bool remove = false;
        {
            RepetitionEntry & entry = _table[zobristHash];
            entry.TotalCount--;
            if (TSearching == IsSearching::Searching)
                entry.SearchCount--;

            if (entry.TotalCount <= 0)
                remove = true;
        }
        if (remove)
            _table.erase(zobristHash);
    }

    void Clear()
    {
        _table.clear();
    }
};