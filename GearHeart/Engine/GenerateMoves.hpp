#pragma once

#include <vector>

#include <boost/container/static_vector.hpp>

#include "../Core.hpp"
#include "PieceMovement.hpp"

namespace MoveGeneration
{

// TODO: pull out of MoveGeneration namespace?
// "color" is the color of the piece on the given square
inline bool CoordIsAttacked(Board const & board, Coord const coord, Color const color)
{
    // try knight moves
    for (Offset offset : GetMoveDirections(Knight))
    {
        Coord testCoord = coord + offset;
        if (testCoord.IsValid() && board[testCoord] == ~color + Knight)
            return true;
    }
    // try pawn moves
    for (Offset offset : PawnCaptureDirections(~color))
    {
        // we got the direction the enemy pawn would be attacking, so we have to walk backward to see if a pawn is in the square where it would attack us from
        Coord testCoord = coord - offset;
        if (testCoord.IsValid() && board[testCoord] == ~color + Pawn)
            return true;
    }
    // Try sliding moves. Break it up by bishop and rook moves, because we want to use the functions _MovesLikeBishop() and _MovesLikeRook().
    for (PieceType movesLike : {Bishop, Rook})
    {
        for (Offset offset : GetMoveDirections(movesLike))
        {
            for (int dist = 1; ; dist++)
            {
                Coord testCoord = coord + (offset*dist);
                // ran off edge
                if (testCoord.IsInvalid())
                    break;

                Piece piece = board[testCoord];
                if (piece == Space)
                    continue;

                // friendly piece
                if (piece.GetColor() == color)
                    break;

                // By the process of elimination, this must be an enemy piece.
                // Non-sliding pieces don't count if we're more than 1 move away.
                if (dist > 1 && !IsSliding(piece))
                    break;

                if (movesLike == Bishop && MovesLikeBishop(piece))
                    return true;
                    
                if (movesLike == Rook && MovesLikeRook(piece))
                    return true;

                // if the piece doesn't contain this particular move, then break
                break;
            }
        }
    }
    
    // if none of the above found anything, then we must not be attacked
    return false;
}

typedef boost::container::static_vector<PieceType, 4> promotion_options_t;

extern promotion_options_t const AllPawnPromotions;
extern promotion_options_t const GoodPawnPromotions;

// Pseudo-legal
template <SearchType TSearchType, typename TVector>
void GenerateNormalPawnMoves(TVector & moves, Board const & board, Color toMove)
{
    // we only want to attempt to promote to queen in quiescence search
    promotion_options_t const & promotionOptions = TSearchType == SearchType::Quiescence ? GoodPawnPromotions : AllPawnPromotions;

    for (Coord srcCoord : board.GetPieceList(toMove + Pawn))
    {
        bool isPromotion = CoordIsPawnPromotion(srcCoord + PawnMovementDirection(toMove), toMove);

        // try to generate captures
        for (Offset captureDirection : PawnCaptureDirections(toMove))
        {
            Coord dstCoord = srcCoord+captureDirection;
            if (dstCoord.IsValid() && board[dstCoord] != Space && board[dstCoord].GetColor() != toMove)
            {
                if (!isPromotion)
                {
                    moves.emplace_back(Constructor<MoveNormal>::Tag, toMove+Pawn, srcCoord, dstCoord, board[dstCoord]);
                }
                else
                {
                    for (PieceType promotionPieceType : promotionOptions)
                    {
                        moves.emplace_back(Constructor<MovePromotion>::Tag, toMove+Pawn, srcCoord, dstCoord, promotionPieceType, board[dstCoord]);
                    }
                }
            }
        }

        // check normal move
        Offset normalMoveDirection = PawnMovementDirection(toMove);
        Coord normalMoveDstCoord = srcCoord+normalMoveDirection;
        if (normalMoveDstCoord.IsValid() && board[normalMoveDstCoord] == Space)
        {
            // quiet move (non-capture, non-promotion)
            if (TSearchType != SearchType::Quiescence &&
                !isPromotion)
            {
                moves.emplace_back(Constructor<MoveNormal>::Tag, toMove+Pawn, srcCoord, normalMoveDstCoord);
            }

            if (isPromotion)
            {
                for (PieceType promotionPieceType : promotionOptions)
                {
                    moves.emplace_back(Constructor<MovePromotion>::Tag, toMove+Pawn, srcCoord, normalMoveDstCoord, promotionPieceType);
                }
            }
        }

        // check double move (which is quiet)
        Coord doubleMoveDstCoord = srcCoord+(normalMoveDirection*2);
        if (TSearchType != SearchType::Quiescence && 
            CoordIsPawnStartingLocation(srcCoord, toMove) && board[normalMoveDstCoord] == Space && board[doubleMoveDstCoord] == Space)
        {
            // can't be capture or promotion
            moves.emplace_back(Constructor<MoveNormal>::Tag, toMove+Pawn, srcCoord, doubleMoveDstCoord);
        }
    }
}

// Pseudo-legal
template <SearchType TSearchType, typename TVector>
void GenerateKnightMoves(TVector & moves, Board const & board, Color toMove)
{
    for (Coord srcCoord : board.GetPieceList(toMove + Knight))
    {
        for (Offset moveDirection : GetMoveDirections(Knight))
        {
            Coord dstCoord = srcCoord + moveDirection;

            // quiet move
            if (TSearchType != SearchType::Quiescence && 
                dstCoord.IsValid() && board[dstCoord] == Space)
            {
                moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + Knight, srcCoord, dstCoord);
            }

            // capture
            if (dstCoord.IsValid() && board[dstCoord] != Space && board[dstCoord].GetColor() != toMove)
            {
                moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + Knight, srcCoord, dstCoord, board[dstCoord]);
            }
        }
    }
}

template <SearchType TSearchType, typename TVector>
void GenerateNormalKingMoves(TVector & moves, Board const & board, Color toMove)
{
    for (Coord srcCoord : board.GetPieceList(toMove + King))
    {
        for (Offset moveDirection : GetMoveDirections(King))
        {
            Coord dstCoord = srcCoord + moveDirection;
            // quiet move
            if (TSearchType != SearchType::Quiescence &&
                dstCoord.IsValid() && board[dstCoord] == Space)
            {
                moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + King, srcCoord, dstCoord, board[dstCoord]);
            }

            // capture
            if (dstCoord.IsValid() && board[dstCoord] != Space && board[dstCoord].GetColor() != toMove)
            {
                moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + King, srcCoord, dstCoord, board[dstCoord]);
            }
        }
    }
}

// Pseudo-legal
template <SearchType TSearchType, typename TVector>
void GenerateBishoplikeMoves(TVector & moves, Board const & board, Color toMove)
{
    for (PieceType pieceType : {Queen, Bishop})
    {
        for (Coord srcCoord : board.GetPieceList(toMove + pieceType))
        {
            for (Offset moveDirection : GetMoveDirections(Bishop))
            {
                for (int dist = 1; ; dist++)
                {
                    Coord dstCoord = srcCoord + (moveDirection*dist);
                    // if we find an invalid coord, or find a coord which has a piece of our color, then we can't slide any further
                    if (dstCoord.IsInvalid() || (board[dstCoord] != Space && board[dstCoord].GetColor() == toMove))
                        break;

                    // otherwise, this should be a valid move

                    // check quiet move
                    if (TSearchType != SearchType::Quiescence &&
                        board[dstCoord] == Space)
                    {
                        moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + pieceType, srcCoord, dstCoord);
                    }

                    // check capture
                    if (board[dstCoord] != Space)
                    {
                        moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + pieceType, srcCoord, dstCoord, board[dstCoord]);
                        // if we captured a piece, we can't slide any further 
                        break;
                    }
                }
            }
        }
    }
}

// Pseudo-legal
template <SearchType TSearchType, typename TVector>
void GenerateRooklikeMoves(TVector & moves, Board const & board, Color toMove)
{
    for (PieceType pieceType : {Queen, Rook})
    {
        for (Coord srcCoord : board.GetPieceList(toMove + pieceType))
        {
            for (Offset moveDirection : GetMoveDirections(Rook))
            {
                for (int dist = 1; ; dist++)
                {
                    Coord dstCoord = srcCoord + (moveDirection*dist);
                    // if we find an invalid coord, or find a coord which has a piece of our color, then we can't slide any further
                    if (dstCoord.IsInvalid() || (board[dstCoord] != Space && board[dstCoord].GetColor() == toMove))
                        break;

                    // otherwise, this should be a valid move

                    // check quiet move
                    if (TSearchType != SearchType::Quiescence &&
                        board[dstCoord] == Space)
                    {
                        moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + pieceType, srcCoord, dstCoord);
                    }

                    // check capture
                    if (board[dstCoord] != Space)
                    {
                        moves.emplace_back(Constructor<MoveNormal>::Tag, toMove + pieceType, srcCoord, dstCoord, board[dstCoord]);
                        // if we captured a piece, we can't slide any further 
                        break;
                    }
                }
            }
        }
    }
}

template <typename TVector>
void GenerateKingsideCastlingMove(TVector & moves, Board const & board, Color toMove)
{
    Coord kingSrcCoord = GetKingStartingCoord(toMove);
    if (board.CanCastleLike(toMove + King) && !CoordIsAttacked(board, GetKingsideCastlingRookDestination(toMove), toMove))
    {
        Offset kingsideDirection = Offset{1, 0};

        // check for clearance
        for (int offsetAmount = 1; offsetAmount <= 2; offsetAmount++)
        {
            Coord testCoord = kingSrcCoord + (kingsideDirection*offsetAmount);
            if (board[testCoord] != Space)
                return;
        }

        // prevent castling out of or through check
        for (int offsetAmount = 0; offsetAmount <= 1; offsetAmount++)
        {
            Coord testCoord = kingSrcCoord + (kingsideDirection*offsetAmount);
            if (CoordIsAttacked(board, testCoord, toMove))
                return;
        }

        moves.emplace_back(
            Constructor<MoveCastling>::Tag,
            toMove + King,
            kingSrcCoord,
            GetKingsideCastlingKingDestination(toMove),
            GetKingsideRookStartingCoord(toMove)
        );
    }
}

template <typename TVector>
void GenerateQueensideCastlingMove(TVector & moves, Board const & board, Color toMove)
{
    Coord kingSrcCoord = GetKingStartingCoord(toMove);
    if (board.CanCastleLike(toMove + Queen) && !CoordIsAttacked(board, GetQueensideCastlingRookDestination(toMove), toMove))
    {
        Offset queensideDirection = Offset{-1, 0};

        // check for clearance
        for (int offsetAmount = 1; offsetAmount <= 3; offsetAmount++)
        {
            Coord testCoord = kingSrcCoord + (queensideDirection*offsetAmount);
            if (board[testCoord] != Space)
                return;
        }

        // prevent castling out of or through check
        for (int offsetAmount = 0; offsetAmount <= 1; offsetAmount++)
        {
            Coord testCoord = kingSrcCoord + (queensideDirection*offsetAmount);
            if (CoordIsAttacked(board, testCoord, toMove))
                return;
        }

        moves.emplace_back(
            Constructor<MoveCastling>::Tag,
            toMove + King,
            kingSrcCoord,
            GetQueensideCastlingKingDestination(toMove),
            GetQueensideRookStartingCoord(toMove)
        );
    }
}

template <typename TVector>
void GenerateEnPassantMoves(TVector & moves, Board const & board, Color toMove)
{
    Coord enPassantCoord = board.GetEnPassantCoord();
    for (Offset offset : PawnCaptureDirections(toMove))
    {
        Coord testCoord = enPassantCoord - offset;
        if (testCoord.IsValid() && board[testCoord] == toMove + Pawn)
        {
            moves.emplace_back(
                Constructor<MoveEnPassant>::Tag,
                toMove + Pawn,
                testCoord,
                enPassantCoord
            );
        }
    }
}

inline bool IsInCheck(Board const & board, Color const color)
{
    return CoordIsAttacked(board, board.GetKingCoord(color), color);
}

inline bool IsMoveLegal(MoveBase const & move, Board & board, Color toMove)
{
    PerformMoveGuard<IsSearching::TestingMove> guard{board, move};
    return !IsInCheck(board, toMove);
}

}

// For now, this is a pseudo-legal move generator
template <SearchType TSearchType, typename TVector>
void GeneratePseudolegalMoves(TVector & moves, Board const & board)
{
    // pawns
    Color toMove = board.GetToMove();
    MoveGeneration::GenerateNormalPawnMoves<TSearchType>(moves, board, toMove);
    if (board.GetEnPassantCoord().IsValid())
        MoveGeneration::GenerateEnPassantMoves(moves, board, toMove);

    // pieces
    MoveGeneration::GenerateKnightMoves<TSearchType>(moves, board, toMove);
    MoveGeneration::GenerateBishoplikeMoves<TSearchType>(moves, board, toMove);
    MoveGeneration::GenerateRooklikeMoves<TSearchType>(moves, board, toMove);

    // king moves and castling (which is quiet)
    MoveGeneration::GenerateNormalKingMoves<TSearchType>(moves, board, toMove);
    if (TSearchType != SearchType::Quiescence &&
        board.CanCastle(toMove))
    {
        MoveGeneration::GenerateKingsideCastlingMove(moves, board, toMove);
        MoveGeneration::GenerateQueensideCastlingMove(moves, board, toMove);
    }
}

template <SearchType TSearchType, typename TVector>
void GenerateLegalMoves(TVector & moves, Board & board)
{
    Color toMove = board.GetToMove();
    GeneratePseudolegalMoves<TSearchType>(moves, board);
    for (int i = 0; i < std::size(moves); i++)
    {
        if (!MoveGeneration::IsMoveLegal(*moves[i], board, toMove))
        {
            // Replace the current move with the final move in the vector, and then pop that (now-duplicate) move off the vector.
            // This still works if we are at the final move in the vector
            moves[i] = moves.back();
            moves.pop_back();
            // ensure we check the same index next iteration
            i--; 
        }
    }
}