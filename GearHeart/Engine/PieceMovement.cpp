
#include "../Engine.hpp"

std::array<MovementInfoData, 7> const MovementInfo{
    MovementInfoData{ // None
        {},
        false,
        false,
        false
    },
    MovementInfoData{ // Pawn
        {},
        false,
        false,
        false
    },
    MovementInfoData{ // Knight
        {
            Offset{ 1,  2},
            Offset{ 2,  1},
            Offset{ 2, -1},
            Offset{ 1, -2},
            Offset{-1, -2},
            Offset{-2, -1},
            Offset{-2,  1},
            Offset{-1,  2}
        },
        false,
        false,
        false
    },
    MovementInfoData{ // Bishop
        {
            Offset{ 1,  1},
            Offset{ 1, -1},
            Offset{-1, -1},
            Offset{-1,  1}
        },
        true, // slides
        true, // moves like bishop
        false // moves like rook
    },
    MovementInfoData{ // Rook
        {
            Offset{ 1,  0},
            Offset{ 0,  1},
            Offset{-1,  0},
            Offset{ 0, -1}
        },
        true,
        false,
        true
    },
    MovementInfoData{ // Queen
        {
            Offset{ 1,  0},
            Offset{ 0,  1},
            Offset{-1,  0},
            Offset{ 0, -1},
            Offset{ 1,  1},
            Offset{ 1, -1},
            Offset{-1, -1},
            Offset{-1,  1}
        },
        true,
        true,
        true
    },
    MovementInfoData{ // King
        {
            Offset{ 1,  0},
            Offset{ 0,  1},
            Offset{-1,  0},
            Offset{ 0, -1},
            Offset{ 1,  1},
            Offset{ 1, -1},
            Offset{-1, -1},
            Offset{-1,  1}
        },
        false,
        true,
        true
    },
};

std::array<PawnMovementInfoData, 2> const PawnMovementInfo{
    PawnMovementInfoData{ // Black
        {
            Offset{-1, -1},
            Offset{ 1, -1}
        },
        Offset{ 0, -1}
    },
    PawnMovementInfoData{ // White
        {
            Offset{-1,  1},
            Offset{ 1,  1}
        },
        Offset{ 0,  1}
    }
};