
#include "../Engine.hpp"

void Engine::Loop()
{
    while (true)
    {
        try
        {
            auto command = _CommandProcessor.WorkerStopAndWaitForCommand();
            switch (command)
            {
            case ProcessorCommand::Search:
                EngineSearch();
                break;
            case ProcessorCommand::Quit:
                return;
            }
        }
        catch (std::exception const & e)
        {
            SyncPrintln("Exception: {0}", e.what());
        }
    }
}

void Engine::CheckFinishedSearching()
{
    // don't check any of the below if we are on infinite mode
    if (_Timing.Infinite)
        return;

    if (_Timing.MaxSearchNodes.has_value() && _Info.NodesVisited >= *_Timing.MaxSearchNodes)
        _finishedSearching = true;

    if (_Timing.ClockStartTime.has_value() && _Timing.SearchDuration.has_value())
    {
        auto elapsedTime = Clock::now() - *_Timing.ClockStartTime;
        if (elapsedTime >= *_Timing.SearchDuration)
            _finishedSearching = true;
    }
}

void Engine::EngineSearch()
{
    _finishedSearching = false;
    _Timing.SearchStartTime = Clock::now();

    _PvTable.Clear();
    // TODO: clear the transposition table (or something). This is important when we do something other than always replace
    // TODO: since this is shared, make sure this only happens in 1 thread
    //_TranspositionTable.Clear();

    boost::container::small_vector<MoveContainer, 64> moves;
    GenerateLegalMoves<SearchType::Normal>(moves, _Board);

    // if no legal moves, then we can't do a lot; see the UCI protocol
    if (moves.empty())
    {
        _Protocol.SendBestMove(SimpleMoveNull);
        return;
    }

    for (int depth = 1; ; depth++)
    {
        _Info.Depth = depth;
        _Info.SelDepth = 0;

        // Let's attempt to do a bit of searching at depth 1, even when we shouldn't.
        // The practical outcome is that we might return a move instead of 0000, and have a chance to not lose.
        if (depth > 1)
        {
            CheckFinishedSearching();

            if (_Timing.MaxSearchDepth.has_value() && depth > *_Timing.MaxSearchDepth)
                _finishedSearching = true;

            if (IsStopping())
                break;
        }

        Score const alpha = -Infinity;
        Score const beta = Infinity;
        Score score = Search<SearchMode::Root>(*this, depth*100, -beta, -alpha, 0);

        // if we're currently in checkmate or stalemate, stop searching, even if we're supposed to be pondering
        if (_PvTable.GetBestMove() == SimpleMoveNull)
            break;

        // don't just break if we find a mate early; we don't want to exit if we are currently pondering
        if (score.IsMate())
        {
            if (_Timing.SearchForMateIn.has_value())
            {
                if (score.MateInMoves() <= *_Timing.SearchForMateIn)
                {
                    _finishedSearching = true;
                }
            }
            // Only exit early normally when the mate-in distance is less than depth.
            // If we don't do this, we might return on a sub-optimal mating distance, which can cause nasty behavior like the engine making repetitions.
            else if (score.MateIn() <= depth)
            {
                _finishedSearching = true;
            }
        }
    }

    _Protocol.SendBestMove(_PvTable.GetBestMove(), _PvTable.GetBestMove(1));
}
