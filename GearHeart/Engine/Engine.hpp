#pragma once

#include <optional>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include "../Core.hpp"

// TODO: implement searchmoves
class Engine
{
public:
    Board _Board;
    PvTable _PvTable;
    TranspositionTable & _TranspositionTable;
    Config const & _Config;
    bool _Debug;
    Info _Info;
    Timing _Timing;
    ProtocolInterface & _Protocol;
    StoppableCommandProcessor _CommandProcessor;

private:
    
    bool _finishedSearching;
    Color _evaluatingForSide;

public:

    Engine(Config const & config, ProtocolInterface & protocol, TranspositionTable & transpositionTable) : 
        _Board{}, 
        _PvTable{},
        _TranspositionTable{transpositionTable},
        _Config{config},
        _Info{}, 
        _Timing{},
        _Protocol{protocol},
        _finishedSearching{false},
        _evaluatingForSide{White}
    {}

    operator ::Board & () {
        return _Board;
    }

    operator ::Board const &  () const
    {
        return _Board;
    }

    void EngineSearch();
    void Loop();
    void CheckFinishedSearching();

    // only stop if we were told to stop by the front-end, or if we are finished searching when not pondering.
    bool IsStopping() const
    {
        if (_CommandProcessor.WorkerIsStopping())
            return true;

        if (!_CommandProcessor.IsPondering() && _finishedSearching)
            return true;

        return false;
    }

    // This needs to be set whenever a new game position is set (not when searching).
    void SetEvaluatingForSide(Color color)
    {
        _evaluatingForSide = color;
    }

    WhiteScore GetDrawScore() const
    {
        // if contempt is 20, that means if we are evaluating as white, we should get a -20 for a draw
        Score contempt = _Config._Contempt;
        return (-contempt).ToWhiteScore(_evaluatingForSide);
    }

    void ConfigUpdated()
    {
        _TranspositionTable.Resize(_Config._HashMb);
    }
};
