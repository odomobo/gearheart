#pragma once

#include "../Core.hpp"
#include "Engine.hpp"

enum class SearchMode
{
    Root,
    Normal,
    Quiescence
};

template <SearchMode searchMode>
Score Search(Engine & engine, int centidepth, Score const originalAlpha, Score const originalBeta, int ply);

template <SearchMode searchMode, typename TVector>
void OrderMoves(Engine const & engine, TVector & moves, TinyMove ttMove, SimpleMove rootMove)
{
    for (OrderedMove & move : moves)
    {
        if (searchMode == SearchMode::Root && *move == rootMove)
        {
            move._Order = Order{3};
        }
        else if (*move == ttMove)
        {
            move._Order = Order{2};
        }
        else if (move->IsCapture())
        {
            // MVV-LVA

            Score victimValue = PieceSquare::GetMaterialValue(move->GetCapturedPiece().GetType());
            int victimSort = victimValue.GetDecipawns();
            // the following is not required, as the victim value can be a max of 90 (queen)
            //victimSort = std::min<int>(victimSort, 255);

            Score attackerValue = PieceSquare::GetMaterialValue(move->GetPiece().GetType());
            // this is required for king as attacker, which would be 3000 (300 pawns)
            int attackerValueDecipawns = std::min<int>(attackerValue.GetDecipawns(), 255);
            int attackerSort = 255 - attackerValueDecipawns; 

            move._Order = Order{1, static_cast<uint8_t>(victimSort), static_cast<uint8_t>(attackerSort)};
        }
        else
        {
            // TODO: something
            move._Order = Order{0};
        }
    }

    // sort descending
    std::sort(moves.rbegin(), moves.rend());
}

template <SearchMode searchMode>
Score Search(Engine & engine, int centiDepth, Score const originalAlpha, Score const originalBeta, int ply)
{
    engine._Info.NodesVisited++;
    if (engine._Info.NodesVisited % Constants::SearchCheckPeriod == 0)
    {
        engine.CheckFinishedSearching();
        if (engine.IsStopping())
            return 0;
    }

    if (searchMode == SearchMode::Quiescence)
        engine._Info.SelDepth = std::max(engine._Info.SelDepth, ply);

    engine._PvTable.ClearPv(ply);

    Board & board = engine._Board;

    if (searchMode != SearchMode::Quiescence && board.IsRepetition())
        return engine.GetDrawScore().FromWhiteScore(board.GetToMove());

    Score alpha = originalAlpha;
    Score beta = originalBeta;

    if (searchMode == SearchMode::Quiescence) 
    {
        // if we're in check, then we can't naively set this value as the new alpha... we really need to perform a real search
        if (MoveGeneration::IsInCheck(board, board.GetToMove()))
        {
            // note that this won't immediately trigger a q-search, since "Search" only calls q-search upon recursion
            return Search<SearchMode::Normal>(engine, centiDepth, alpha, beta, ply);
        }

        Score tmpScore = Evaluate(engine).FromWhiteScore(board.GetToMove());

        // TODO: uncomment, to give engine knowledge about endgame drawing
        //Score drawScore = engine.GetDrawScore().FromWhiteScore(board.GetToMove());
        //// prevent the engine from going into a drawn endgame from a winning position
        //if (!board.CanWin(board.GetToMove()))
        //{
        //    tmpScore = Score::Min(drawScore, tmpScore); // we want a high score value
        //    beta = Score::Min(drawScore, beta); // if tmpScore reaches drawscore, then we can't do any better so we should exit early
        //}

        if (tmpScore >= beta)
        {
            // Not necessary, since we've just cleared the PV entry
            //engine._PvTable.ClearPv(ply);
            return beta;
        }

        if (tmpScore > alpha)
        {
            alpha = tmpScore;
        }
    }

    ZobristHash const hash = board.GetZobristHash();
    // Probe TT.
    // For now, let's not probe in qsearch.
    TinyMove ttMove{TinyMoveNull};
    if (searchMode != SearchMode::Quiescence)
    {
        auto optTtEntry = engine._TranspositionTable.TryGetEntry(hash);
        if (optTtEntry.has_value())
        {
            auto ttEntry = *optTtEntry;
            // If we return a value at the root, then we won't save the best move for the PV.
            // If we return a value at ply 0 or 1, we might miss draw by repetition.
            if (ttEntry._CentiDepth >= centiDepth && searchMode != SearchMode::Root && ply > 1)
            {
                Score ttScore = ttEntry._Score.GetIncreasedDistanceToMateBy(ply);
                switch (ttEntry._NodeType)
                {
                case TTEntryType::AllNode:
                    // if the all node failed even lower before, then we can safely fail low here
                    if (ttScore <= alpha)
                        return alpha;
                    break;
                case TTEntryType::PvNode:
                    // normal logic here
                    if (ttScore >= beta)
                        return beta;
                    if (ttScore > alpha)
                    {
                        // set the PV value
                        // TODO: fix this; it seems to be returning bad values, even for non-promotions
                        // Why is it returning bad values???
                        //engine._PvTable.SetPv(ply, static_cast<SimpleMove>(ttEntry.Move));
                        return ttScore;
                    }
                    return alpha;
                    break;
                case TTEntryType::CutNode:
                    // if the cut node failed even higher before, then we can safely fail high here
                    if (ttScore >= beta)
                        return beta;
                    break;
                }
            }
            ttMove = ttEntry._Move;
        }
    }

    // Get the root move from the PV table, just in case the transposition table doesn't have the best move.
    SimpleMove rootMove = SimpleMoveNull;
    if (searchMode == SearchMode::Root)
        rootMove = engine._PvTable.GetBestMove();

    boost::container::small_vector<OrderedMove, 64> pseudolegalMoves;
    if (searchMode == SearchMode::Quiescence)
        GeneratePseudolegalMoves<SearchType::Quiescence>(pseudolegalMoves, board);
    else
        GeneratePseudolegalMoves<SearchType::Normal>(pseudolegalMoves, board);

    OrderMoves<searchMode>(engine, pseudolegalMoves, ttMove, rootMove);
    
    bool anyLegalMoves = false;
    SimpleMove bestMove{SimpleMoveNull};
    for (auto & move : pseudolegalMoves)
    {
        PerformMoveGuard<IsSearching::Searching> guard{board, *move};

        // if the move just put that side into check, it wasn't a legal move
        if (MoveGeneration::IsInCheck(board, ~board.GetToMove()))
            continue;
        else
            anyLegalMoves = true;

        int const nextCentidepth = centiDepth - 100;

        Score tmpScore;
        if (searchMode == SearchMode::Quiescence || nextCentidepth <= 0)
            tmpScore = -Search<SearchMode::Quiescence>(engine, nextCentidepth, -beta, -alpha, ply + 1);
        else
            tmpScore = -Search<SearchMode::Normal>(engine, nextCentidepth, -beta, -alpha, ply + 1);

        if (engine.IsStopping())
        {
            if (searchMode == SearchMode::Root)
                engine._Protocol.SendInfo(engine._Info.LastRootPvScore);

            return 0;
        }

        if (tmpScore >= beta)
        {
            engine._PvTable.ClearPv(ply);

            if (searchMode != SearchMode::Quiescence)
            {
                // save to tt
                engine._TranspositionTable.MaybeSetEntry(hash, beta, TTEntryType::CutNode, *move, centiDepth, ply);
            }

            return beta;
        }

        if (tmpScore > alpha)
        {
            alpha = tmpScore;
            bestMove = *move;
            engine._PvTable.SetPv(ply, *move);
            if constexpr (searchMode == SearchMode::Root)
            {
                engine._Info.LastRootPvScore = tmpScore;
                engine._PvTable.SetBestLine(*move);
                engine._Protocol.SendInfo(alpha);
            }
        }
    }

    if (searchMode != SearchMode::Quiescence && !anyLegalMoves)
    {
        // check if checkmate or stalemate
        if (MoveGeneration::IsInCheck(board, board.GetToMove()))
        {
            // the other player is mating, and it'll take "ply" plies from the root
            return -Score::CreateMateIn(ply);
        }
        else
        {
            return engine.GetDrawScore().FromWhiteScore(board.GetToMove());
        }
    }

    if (searchMode == SearchMode::Root)
    {
        engine._Protocol.SendInfo(alpha);
    }

    // save to tt
    if (searchMode != SearchMode::Quiescence)
    {
        bool isPvNode = alpha > originalAlpha;
        engine._TranspositionTable.MaybeSetEntry(hash, alpha, isPvNode ? TTEntryType::PvNode : TTEntryType::AllNode, bestMove, centiDepth, ply);
    }

    return alpha;
}
