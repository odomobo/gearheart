#pragma once

#include "../Core.hpp"

class Engine;

WhiteScore Evaluate(Board const & board);
WhiteScore EvaluatePartialScore(Board const & board);
WhiteScore AddPiecePartialScoreDiff(Piece piece, Coord coord);
WhiteScore MovePiecePartialScoreDiff(Piece piece, Coord src, Coord dst);
WhiteScore RemovePiecePartialScoreDiff(Piece piece, Coord coord);

bool IsEndgame(Board const & board, Color const color);
bool IsSoloKing(Board const & board, Color const color);