
#include "GenerateMoves.hpp"

MoveGeneration::promotion_options_t const MoveGeneration::AllPawnPromotions{Queen, Knight, Rook, Bishop};
MoveGeneration::promotion_options_t const MoveGeneration::GoodPawnPromotions{Queen};
