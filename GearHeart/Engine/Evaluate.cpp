#include <iterator>

#include "../Engine.hpp"

// assumes each side has a king
WhiteScore Evaluate(Board const & board)
{
    WhiteScore partialScore = board._IncrementalData._PartialScore;

    // endgame evaluation
    WhiteScore endgameKingPieceSquareValue = 0;
    for (Color color : Colors)
    {
        if (IsEndgame(board, color))
        {
            Coord kingCoord = board.GetPieceList(color + King)[0];
            endgameKingPieceSquareValue += PieceSquare::GetPieceSquareValue(PieceSquare::KingEndGameDiff, kingCoord, color);
        }
    }

    WhiteScore tempo = PieceSquare::Tempo.ToWhiteScore(board.GetToMove());

    WhiteScore score = partialScore + endgameKingPieceSquareValue + tempo;

    return score;
}

WhiteScore EvaluatePartialScore(Board const & board)
{
    WhiteScore materialValue = 0;
    WhiteScore pieceSquareValue = 0;

    for (Color color : Colors)
    {
        for (PieceType type : PieceTypes)
        {
            Piece piece = color + type;
            auto const & pieceList = board.GetPieceList(piece);
            materialValue += PieceSquare::GetMaterialValue(piece) * std::size(pieceList);

            for (Coord coord : pieceList)
            {
                pieceSquareValue += PieceSquare::GetPieceSquare(coord, piece);
            }
        }
    }

    return materialValue + pieceSquareValue;
}

WhiteScore AddPiecePartialScoreDiff(Piece piece, Coord coord)
{
    WhiteScore materialValue = PieceSquare::GetMaterialValue(piece);
    WhiteScore pieceSquareValue = PieceSquare::GetPieceSquare(coord, piece);
    return materialValue + pieceSquareValue;
}

WhiteScore MovePiecePartialScoreDiff(Piece piece, Coord src, Coord dst)
{
    WhiteScore srcPieceSquareValue = PieceSquare::GetPieceSquare(src, piece);
    WhiteScore dstPieceSquareValue = PieceSquare::GetPieceSquare(dst, piece);
    return dstPieceSquareValue - srcPieceSquareValue;
}

WhiteScore RemovePiecePartialScoreDiff(Piece piece, Coord coord)
{
    return -AddPiecePartialScoreDiff(piece, coord);
}

bool IsEndgame(Board const & board, Color const color)
{
    // if we want to see if it's an endgame for white, we need to check black's remaining material
    auto minorPieces = board.CountPieces(~color+Knight) + board.CountPieces(~color+Bishop);
    auto rooks = board.CountPieces(~color+Rook);
    auto queens = board.CountPieces(~color+Queen);

    if (queens > 1)
        return false;

    // a single queen and more than a single minor piece
    if (queens == 1 && (rooks > 0 || minorPieces > 1))
        return false;

    // 2 rooks and at least 2 minor pieces (similar logic to above)
    if (queens == 0 && rooks >= 2 && minorPieces > 1)
        return false;

    return true;
}

// currently unused, but maybe useful
bool IsSoloKing(Board const & board, Color const color)
{
    return board.CountPiecesByColor(color) == 1;
}