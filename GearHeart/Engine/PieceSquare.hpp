#pragma once

#include <array>

#include "../Core.hpp"

namespace PieceSquare
{

typedef std::array<Score, 64> PieceSquareTable;

// We could use TinyScores to store these to use less l1 cache, but this uses only 3.5KB and provides faster access time
extern PieceSquareTable const Pawn;
extern PieceSquareTable const Knight;
extern PieceSquareTable const Bishop;
extern PieceSquareTable const Rook;
extern PieceSquareTable const Queen;
extern PieceSquareTable const KingMiddleGame;
extern PieceSquareTable const KingEndGame;
extern PieceSquareTable const KingEndGameDiff;

extern std::array<Score, 7> MaterialValues;

constexpr Score const Tempo{20}; // tempo has to be pretty big with no quiescence search

inline size_t CoordToPieceSquareIndex(Coord coord, Color const color)
{
    // The piece square tables are ordered like they are for black (index 0-7 is a8-h8), but scored as from white
    if (color == White)
    {
        coord = coord.GetFlipped();
    }
    // the indexes are stored in rank-major order
    return coord.Get64ArrayIndex();
}

inline Score GetPieceSquare(size_t index, PieceType const pieceType)
{
    if (pieceType == ::Pawn)
        return Pawn[index];
    if (pieceType == ::Knight)
        return Knight[index];
    if (pieceType == ::Bishop)
        return Bishop[index];
    if (pieceType == ::Rook)
        return Rook[index];
    if (pieceType == ::Queen)
        return Queen[index];
    if (pieceType == ::King)
        return KingMiddleGame[index];

    Unreachable();
}

inline WhiteScore GetPieceSquare(Coord const coord, Piece const piece)
{
    size_t index = CoordToPieceSquareIndex(coord, piece.GetColor());
    Score tmpScore = GetPieceSquare(index, piece.GetType());
    return tmpScore.ToWhiteScore(piece.GetColor());
}

inline WhiteScore GetPieceSquareValue(PieceSquareTable const & pieceSquareTable, Coord const coord, Color const color)
{
    size_t index = CoordToPieceSquareIndex(coord, color);
    Score tmpScore = pieceSquareTable[index];
    return tmpScore.ToWhiteScore(color);
}

inline Score GetMaterialValue(PieceType const type)
{
    return MaterialValues[type.GetIndex()];
}

inline WhiteScore GetMaterialValue(Piece const piece)
{
    Score value = GetMaterialValue(piece.GetType());
    return value.ToWhiteScore(piece.GetColor());
}

inline PieceSquareTable Subtract(PieceSquareTable const & subtrahend, PieceSquareTable const & minuend)
{
    PieceSquareTable ret;
    for (size_t i = 0; i < 64; i++)
    {
        ret[i] = subtrahend[i] - minuend[i];
    }
    return ret;
}

inline PieceSquareTable Multiply(PieceSquareTable const & m1, int m2)
{
    PieceSquareTable ret;
    for (size_t i = 0; i < 64; i++)
    {
        ret[i] = m1[i] * m2;
    }
    return ret;
}

}