#pragma once

#include <array>
#include <boost/container/static_vector.hpp>

#include "../Core.hpp"

typedef boost::container::static_vector<Offset, 8> move_directions_vector_t;

struct MovementInfoData
{
    move_directions_vector_t _MoveDirections;
    bool const _IsSliding;
    bool const _MovesLikeBishop;
    bool const _MovesLikeRook;

    MovementInfoData() = delete;
};

extern std::array<MovementInfoData, 7> const MovementInfo;

inline move_directions_vector_t const & GetMoveDirections(PieceType const type)
{
    return MovementInfo[static_cast<size_t>(type)]._MoveDirections;
}

inline move_directions_vector_t const & GetMoveDirections(Piece const piece)
{
    return GetMoveDirections(piece.GetType());
}

inline bool IsSliding(PieceType const type)
{
    return MovementInfo[static_cast<size_t>(type)]._IsSliding;
}

inline bool IsSliding(Piece const piece)
{
    return IsSliding(piece.GetType());
}

inline bool MovesLikeBishop(PieceType const type)
{
    return MovementInfo[static_cast<size_t>(type)]._MovesLikeBishop;
}

inline bool MovesLikeBishop(Piece const piece)
{
    return MovesLikeBishop(piece.GetType());
}

inline bool MovesLikeRook(PieceType const type)
{
    return MovementInfo[static_cast<size_t>(type)]._MovesLikeRook;
}

inline bool MovesLikeRook(Piece const piece)
{
    return MovesLikeRook(piece.GetType());
}


struct PawnMovementInfoData
{
    std::array<Offset, 2> const CaptureDirections;
    Offset const Movement;

    PawnMovementInfoData() = delete;
};

extern std::array<PawnMovementInfoData, 2> const PawnMovementInfo;

inline std::array<Offset, 2> const & PawnCaptureDirections(Color const color)
{
    return PawnMovementInfo[color.GetIndex()].CaptureDirections;
}

inline Offset PawnMovementDirection(Color const color)
{
    return PawnMovementInfo[color.GetIndex()].Movement;
}

inline bool CoordIsPawnPromotion(Coord const coord, Color const color)
{
    if (color == White && coord.GetRank() == 7)
        return true;

    if (color == Black && coord.GetRank() == 0)
        return true;

    return false;
}

inline bool CoordIsPawnStartingLocation(Coord const coord, Color const color)
{
    if (color == White && coord.GetRank() == 1)
        return true;

    if (color == Black && coord.GetRank() == 6)
        return true;

    return false;
}

inline Coord GetKingStartingCoord(Color const color)
{
    return color == White ? e1 : e8;
}

inline Coord GetKingsideRookStartingCoord(Color const color)
{
    return color == White ? h1 : h8;
}

inline Coord GetKingsideCastlingKingDestination(Color const color)
{
    return color == White ? g1 : g8;
}

inline Coord GetKingsideCastlingRookDestination(Color const color)
{
    return color == White ? f1 : f8;
}

inline Coord GetQueensideRookStartingCoord(Color const color)
{
    return color == White ? a1 : a8;
}

inline Coord GetQueensideCastlingKingDestination(Color const color)
{
    return color == White ? c1 : c8;
}

inline Coord GetQueensideCastlingRookDestination(Color const color)
{
    return color == White ? d1 : d8;
}