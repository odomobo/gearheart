## About

GearHeart is a UCI-compliant C++17 chess engine. It's designed to be 2 things:

1. A didactic reference implementation of a simple mailbox chess engine.
2. The base for your next chess engine project. It has a very leniant license, so you aren't required to share your code if you make any modifications. You own the code, and can do with it what you wish. See the included license for details.

There are a few guiding principles I used when designing this project:

1. Separate the project into a hierarchy of modules. Each module depends on the module below. This helps prevent the entire project from becoming tightly coupled (i.e. it prevents spaghetti).
2. Use strong types for the base types. There are classes for Color, PieceType, Piece (combination of Color and PieceType), Coord, Offset (where Coord + Offset -> Coord). These are really wrappers around native types, usually int. Using these types ensures you can't accidentally call a function with the wrong type. The compiler should optimize their usage down to native operations around their underlying type.
3. Optimize the engine, and don't optimize the interface. The interface methods are called maybe once per several seconds, and the engine methods are called millions of times per second. A little bit of overhead in the interface won't impact the engine's speed.
4. Make everything as simple as possible, and no simpler. I intentionally left out easy optimizations, while keeping everything inherently as efficient as possible. The engine can be made very strong by a combination of smart optimizations (which add complexity) and a more comprehesive evaluation.